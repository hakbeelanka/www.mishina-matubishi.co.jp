import React, { Component } from "react"
import {Link} from "react-router-dom"

export default class TechnologyListEn extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount(){
  }

  render(){
    return (
      <div className={`technology_list`}>
        <ul>
          <li className={`rerolling`}>
            <Link to={`/en/technologies/rerolling`} >
              <div>
                <h3>REROLLING</h3>
              </div>
            </Link>
            <div>
              <h4>Rerolling</h4>
              <p>Thickness is in our hands.</p>
            </div>
          </li>
          <li className={`annealing`}>
            <Link to={`/en/technologies/annealing`} >
              <div>
                <h3>ANNEALING</h3>
              </div>
            </Link>
            <div>
              <h4>Annealing</h4>
              <p>We keep precise control of the heat applied.</p>
            </div>
          </li>
          <li className={`pickling`}>
            <Link to={`/en/technologies/pickling`} >
              <div>
                <h3>PICKLING</h3>
              </div>
            </Link>
            <div>
              <h4>Pickling</h4>
              <p>Refining metals with acid.</p>
            </div>
          </li>
          <li className={`cutting`}>
            <Link to={`/en/technologies/cutting`} >
              <div>
                <h3>LEVELING & CUTTING</h3>
              </div>
            </Link>
            <div>
              <h4>Levelling & Cutting</h4>
              <p>We offer a diverse variety of cutting.</p>
            </div>
          </li>
          <li className={`shearing`}>
            <Link to={`/en/technologies/shearing`} >
              <div>
                <h3>SHEARING</h3>
              </div>
            </Link>
            <div>
              <h4>Shearing</h4>
              <p>The power to cut steel in one fell swoop.</p>
            </div>
          </li>
          <li className={`slit`}>
            <Link to={`/en/technologies/slit`} >
              <div>
                <h3>SLIT</h3>
              </div>
            </Link>
            <div>
              <h4>Slitting</h4>
              <p>We work with steel with the flexibility of paper.</p>
            </div>
          </li>
          <li className={`printer`}>
            <Link to={`/en/technologies/printer`} >
              <div>
                <h3>3D PRINTING</h3>
              </div>
            </Link>
            <div>
              <h4>3D Printing</h4>
              <p>Creating 3D shapes with masterful skill.</p>
            </div>
          </li>
          <li className={`analysis`}>
            <Link to={`/en/technologies/analysis`} >
              <div>
                <h3>ANALYSIS & TESTING</h3>
              </div>
            </Link>
            <div>
              <h4>Testing and Inspection</h4>
              <p>We offer you peace of mind.</p>
            </div>
          </li>
          <li className={`sheetmetal`}>
            <Link to={`/en/technologies/sheetmetal`} >
              <div>
                <h3>SHEET METAL WORKING</h3>
              </div>
            </Link>
            <div>
              <h4>Sheet metal processing</h4>
              <p>Changing the form.</p>
            </div>
          </li>
        </ul>
        <div className={`image-cache`}>
          <img src="/assets/images/technologies/icons/analysis_col.png" alt="" />
          <img src="/assets/images/technologies/icons/annealing_col.png" alt="" />
          <img src="/assets/images/technologies/icons/cutting_col.png" alt="" />
          <img src="/assets/images/technologies/icons/pickling_col.png" alt="" />
          <img src="/assets/images/technologies/icons/printer_col.png" alt="" />
          <img src="/assets/images/technologies/icons/rerolling_col.png" alt="" />
          <img src="/assets/images/technologies/icons/shearing_col.png" alt="" />
          <img src="/assets/images/technologies/icons/slit_col.png" alt="" />
          <img src="/assets/images/technologies/icons/sheetmetal_col.png" alt="" />
        </div>
      </div>
    )
  }

}