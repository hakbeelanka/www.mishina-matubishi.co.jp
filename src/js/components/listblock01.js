import React, { Component } from "react"
import ClassNames from "classnames"

export default class ListBlock01 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      theme: this.props.theme
    }
  }

  createMarkup(parag) { return {__html: parag}; };

  render() {

    let listClass = ClassNames(
      "listblock01",
      {
        "theme-transparent": this.state.theme === "transparent",
        "theme-white": this.state.theme === "white"
      }
    )

    return (
      <ul className={listClass}>
        {
          this.state.data.map((item, i) => {
            return (
              <li key={i}>
                <div className="title">
                  <h3>{item.title}</h3>
                </div>
                <div className="content" dangerouslySetInnerHTML={this.createMarkup(item.content)} />
              </li>
            )
          })
        }
      </ul>
    )
  }

}