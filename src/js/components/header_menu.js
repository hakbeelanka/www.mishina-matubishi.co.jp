import React, {Component} from "react"
import {NavLink} from "react-router-dom"
import ClassNames from "classnames"

export default class HeaderMenu extends Component {

  constructor(props) {
    super(props)
    this.state = {
      menu: [],
      isWhite: false,
      lang: "/"
    }


    this.toggleMenuOnMobileHandleClick = this.toggleMenuOnMobileHandleClick.bind(this)
    this.closeMenuOnMobileHandleClick = this.closeMenuOnMobileHandleClick.bind(this)
  }

  componentWillMount(){
    if(window.location.pathname === '/philosophy'){
      this.setState({
        isWhite: true
      })
    } else {
      this.setState({
        isWhite: false
      })
    }
  }

  componentDidMount() {
    this._renderMenus(this.props.menu);
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.menu.length && nextProps.menu.length) {
      this._renderProjects(nextProps.menu);
    }
    if(window.location.pathname === '/philosophy'){
      this.setState({
        isWhite: true
      })
    } else {
      this.setState({
        isWhite: false
      })
    }
  }

  _renderMenus(menu) {
    this.setState({menu: menu})
  }

  toggleMenuOnMobileHandleClick(){
    this.setState({isOpen: !this.state.isOpen});
  }

  closeMenuOnMobileHandleClick(){
    this.setState({isOpen: false});
  }

  render() {

    let MenuColorCls = ClassNames("header_menu",{
      "is-white": this.state.isWhite,
      "is-open": this.state.isOpen
    })

    return (
      <div className={MenuColorCls}>
        <button className={`btn__open`} onClick={this.toggleMenuOnMobileHandleClick} />
        <header>
          <div className="header__inner">
            <ul>
              <li onClick={this.closeMenuOnMobileHandleClick}><NavLink to={`${this.props.lang}philosophy/`} data-active={this.props.lang === '/' ? "私たちの鉄則" : "PHILOSOPHY"}>PHILOSOPHY</NavLink></li>
              <li onClick={this.closeMenuOnMobileHandleClick}><NavLink to={`${this.props.lang}vision/`} data-active={this.props.lang === '/' ? "未来への展望" : "VISION"}>VISION</NavLink></li>
              <li onClick={this.closeMenuOnMobileHandleClick}><NavLink to={`${this.props.lang}technologies/`} data-active={this.props.lang === '/' ? "技術紹介" : "TECHNOLOGY"}>TECHNOLOGY</NavLink></li>
              <li onClick={this.closeMenuOnMobileHandleClick}><NavLink to={`${this.props.lang}products/`} data-active={this.props.lang === '/' ? "製品について" : "PRODUCTS"}>PRODUCTS</NavLink></li>
            </ul>
            <div className="logo">
              <NavLink to={`${this.props.lang}`}  onClick={this.closeMenuOnMobileHandleClick} />
            </div>
            <ul>
              <li onClick={this.closeMenuOnMobileHandleClick}><NavLink to={`${this.props.lang}history`} data-active={this.props.lang === '/' ? "三品松菱のあゆみ" : "HISTORY"}>HISTORY</NavLink></li>
              <li onClick={this.closeMenuOnMobileHandleClick}><NavLink to={`${this.props.lang}about`} data-active={this.props.lang === '/' ? "会社概要" : "ABOUT US"}>ABOUT US</NavLink></li>
              <li onClick={this.closeMenuOnMobileHandleClick}><NavLink to={`${this.props.lang}recruit`} data-active={this.props.lang === '/' ? "採用情報" : "CAREER"}>{this.props.lang === '/' ? "RECRUIT" : "CAREER"}</NavLink></li>
              <li onClick={this.closeMenuOnMobileHandleClick}><a href="https://www.mishina-and-co.com/miraisouzou/" target="_blank" data-active={this.props.lang === '/' ? "未来創造PJ" : "FUTURE"}>{this.props.lang === '/' ? "FUTURE" : "FUTURE"}</a></li>
            </ul>
            {/*<div className="menu_sns">*/}
              {/*<a className="instagram" href=""></a>*/}
              {/*<a className="facebook" href=""></a>*/}
            {/*</div>*/}
          </div>
        </header>
        {this.props.children}
      </div>
    )
  }
}