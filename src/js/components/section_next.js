import React, {Component} from "react"
import Style from "style-it"
import Particles from 'react-particles-js'
import {Link} from 'react-router-dom'

export default class SectionNext extends Component {

  constructor(props) {
    super(props)
    this.state = {
      current: this.props.current,
      nextid: this.props.nextid,
      title: this.props.title,
      sub: this.props.sub
    }
  }

  componentDidMount() {

  }

  render() {
    return (
      <section className="section_next">
        <Link to={this.props.nextPath}>
          <Style>{`
          .section_next:after {
            background-image: url("/assets/images/${this.state.current}/next.png");
          }
        `}</Style>
          <Particles
            params={{
              particles: {
                line_linked: {
                  color: "#bababb"
                },
                color: {
                  value: "#bababb"
                },
                shape: {
                  stroke: {
                    color: "#bababb"
                  }
                },
                size: {
                  value: 10,
                  random: true,
                  anim: {
                    enable: false,
                    speed: 80,
                    size_min: 0.1,
                    sync: false
                  }
                },
              }
            }}
          />
          <div className="container">
            <span className="next">NEXT</span>
            <div className="heading" data-index={this.state.nextid}>
              <h2>{this.state.title}</h2>
              <h3>{this.state.sub}</h3>
            </div>
          </div>
        </Link>
      </section>
    )
  }

}

