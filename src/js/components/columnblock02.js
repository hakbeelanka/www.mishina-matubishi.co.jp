import React, { Component } from "react"
import ClassNames from "classnames"

export default class ColumnBlock02 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      theme: this.props.theme ? this.props.theme : false,
      reverse: this.props.reverse ? this.props.reverse : false,
      wrap01: this.props.wrap01,
      wrap02: this.props.wrap02,
    }
  }

  render() {

    let leadClass = ClassNames(
      "columnblock02",
      {
        "is-reverse": this.state.reverse
      }
    )

    return (
      <div className={leadClass}>
        <div className="columnblock02__wrap01">
          {this.props.wrap01}
        </div>
        <div className="columnblock02__wrap02">
          {this.props.wrap02}
        </div>
      </div>
    )
  }

}