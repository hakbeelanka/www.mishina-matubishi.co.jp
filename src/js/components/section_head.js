import React, { Component } from "react"
import ClassNames from "classnames"

export default class SectionHead extends Component {

  constructor(props) {
    super(props)
    this.state = {
      theme: this.props.theme,
      poster: this.props.poster,
      title: this.props.title,
      sub: this.props.sub
    }
  }

  componentDidMount(){

  }

  render(){

    let headClass = ClassNames(
      "section_head",
      {
        "theme-black": this.state.theme === "black",
        "theme-white": this.state.theme === "white"
      })
    return (
      <section className={headClass}
               style={{backgroundImage: `url(${this.state.poster})`}}
      >
        <div className="heading">
          <h1>{this.state.title}</h1>
          <p>{this.state.sub}</p>
        </div>
      </section>
    )
  }

}

