import React, { Component } from "react"
import $ from "jquery"

export default class BackToTop extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }

    this.scrollTopOnClick = this.scrollTopOnClick.bind(this)
  }

  scrollTopOnClick(){
    $('body,html').animate({scrollTop: 0},1000);
  }

  render(){
    return (
      <div className="backtotop">
        <div className="backtotop__container">
          <button onClick={this.scrollTopOnClick}>TOP</button>
        </div>
      </div>
    )
  }

}