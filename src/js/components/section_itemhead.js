import React, { Component } from "react"

export default class SectionItemHead extends Component {

  constructor(props) {
    super(props)
    this.state = {
      poster: this.props.poster,
      title: this.props.title,
      sub: this.props.sub
    }
  }

  componentDidMount(){

  }

  render(){

    const detector = () => {
      if (this.state.poster.slice(-3) === 'mp4'){
        return (
          <video autoPlay={true} playsInline={true} loop muted>
            <source src={this.state.poster} type="video/mp4" />
          </video>
        )
      } else {
        return (
          <div className="poster" style={{backgroundImage: `url(${this.state.poster})`}} />
        )
      }
    }

    return (
      <section className={`section_itemhead`}>
        {detector()}
        <div className="heading">
          <h1>{this.state.title}</h1>
          <p>{this.state.sub}</p>
        </div>
      </section>
    )
  }

}

