import { Component } from 'react'
import { withRouter } from 'react-router-dom'
import $ from 'jquery'

class ScrollToTop extends Component {
  componentDidUpdate(prevProps) {
    // console.log(this.props.location,prevProps.location)
    if (this.props.location !== prevProps.location) {
      $('body,html').animate({scrollTop: 0},0);
    }
  }

  render() {
    return this.props.children;
  }
}

export default withRouter(ScrollToTop);