import React, { Component } from "react"
import ClassNames from "classnames"

export default class Interview extends Component {

  constructor(props) {
    super(props);
    this.state = {
      index: this.props.index + 1,
      data: this.props.data
    }
  }

  createMarkup(parag) { return {__html: parag}; };

  render() {

    let interviewClass = ClassNames(
      "interview",
      `index-${this.state.index}`
    )

    return (
      <div className={interviewClass}>
        <div className="interview__person">
          <div data-index={'0' + this.state.index}>
            <h2>{this.state.data.division}</h2>
            <h3 dangerouslySetInnerHTML={this.createMarkup(this.state.data.keywords)} />
            <div className="personal_data">
              <h4>{this.state.data.name_en}</h4>
              <p className="joined">Joined the company in {this.state.data.joined}</p>
              <p dangerouslySetInnerHTML={this.createMarkup(this.state.data.career)} />
            </div>
          </div>
          <figure style={{backgroundImage: `url("/assets/images/recruit/interview${'0'+this.state.index}-01.png")`}} />
        </div>
        <div className="interview__comment">
          <figure style={{backgroundImage: `url("/assets/images/recruit/interview${'0'+this.state.index}-02.png")`}} />
          <div>
            <p>{this.state.data.comment}</p>
          </div>
        </div>
        <div className="interview__mind">
          <div>
            <h3>My goals</h3>
            <p>{this.state.data.goal}</p>
          </div>
          <div>
            <h3>What's appealing about Mishina Matsubishi</h3>
            <p>{this.state.data.reptune}</p>
          </div>
        </div>
      </div>
    )
  }

}