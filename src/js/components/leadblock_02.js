import React, { Component } from "react"
import ClassNames from "classnames"

export default class LeadBlock02 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title ? this.props.title : null,
      theme: this.props.theme ? this.props.theme : null,
      render: this.props.render ? this.props.render : this.props.children,
      last: this.props.last ? this.props.last : false
    }
  }

  render() {

    let leadClass = ClassNames(
      "leadblock02",
      {
        "theme-gold": this.state.theme === "gold",
        "last": this.state.last
      }
    )

    return (
      <div className={leadClass}>
        {
          this.props.title ?
            (
              <div className="leadblock02__heading">
                <h2>{this.state.title}</h2>
              </div>
            ) :
            null
        }
        <div className="leadblock02__content">
          {this.props.render}
        </div>
      </div>
    )
  }

}