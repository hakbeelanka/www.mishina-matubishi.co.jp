import React, { Component } from "react"
import {Link} from "react-router-dom"

export default class TechnologyList extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount(){
  }

  render(){
    return (
      <div className={`technology_list`}>
        <ul>
          <li className={`rerolling`}>
            <Link to={`/technologies/rerolling`} >
              <div>
                <h3>REROLLING</h3>
              </div>
            </Link>
            <div>
              <h4>圧延</h4>
              <p>厚みを操る。</p>
            </div>
          </li>
          <li className={`annealing`}>
            <Link to={`/technologies/annealing`} >
              <div>
                <h3>ANNEALING</h3>
              </div>
            </Link>
            <div>
              <h4>焼鈍</h4>
              <p>熱を制する。</p>
            </div>
          </li>
          <li className={`pickling`}>
            <Link to={`/technologies/pickling`} >
              <div>
                <h3>PICKLING</h3>
              </div>
            </Link>
            <div>
              <h4>酸洗</h4>
              <p>酸でみがく。</p>
            </div>
          </li>
          <li className={`cutting`}>
            <Link to={`/technologies/cutting`} >
              <div>
                <h3>LEVELING & CUTTING</h3>
              </div>
            </Link>
            <div>
              <h4>レベラーカット</h4>
              <p>多彩な切り出し。</p>
            </div>
          </li>
          <li className={`shearing`}>
            <Link to={`/technologies/shearing`} >
              <div>
                <h3>SHEARING</h3>
              </div>
            </Link>
            <div>
              <h4>シャーリング</h4>
              <p>鉄を断つ。</p>
            </div>
          </li>
          <li className={`slit`}>
            <Link to={`/technologies/slit`} >
              <div>
                <h3>SLIT</h3>
              </div>
            </Link>
            <div>
              <h4>スリット</h4>
              <p>鉄を裂く。</p>
            </div>
          </li>
          <li className={`printer`}>
            <Link to={`/technologies/printer`} >
              <div>
                <h3>3D PRINTING</h3>
              </div>
            </Link>
            <div>
              <h4>３Ｄプリンター</h4>
              <p>巧みな三次元造形。</p>
            </div>
          </li>
          <li className={`analysis`}>
            <Link to={`/technologies/analysis`} >
              <div>
                <h3>ANALYSIS & TESTING</h3>
              </div>
            </Link>
            <div>
              <h4>試験・検査</h4>
              <p>安心を提供する。</p>
            </div>
          </li>
          <li className={`sheetmetal`}>
            <Link to={`/technologies/sheetmetal`} >
              <div>
                <h3>SHEET METAL WORKING</h3>
              </div>
            </Link>
            <div>
              <h4>板金加工</h4>
              <p>形状を変化。</p>
            </div>
          </li>
        </ul>
        <div className={`image-cache`}>
          <img src="/assets/images/technologies/icons/analysis_col.png" alt="" />
          <img src="/assets/images/technologies/icons/annealing_col.png" alt="" />
          <img src="/assets/images/technologies/icons/cutting_col.png" alt="" />
          <img src="/assets/images/technologies/icons/pickling_col.png" alt="" />
          <img src="/assets/images/technologies/icons/printer_col.png" alt="" />
          <img src="/assets/images/technologies/icons/rerolling_col.png" alt="" />
          <img src="/assets/images/technologies/icons/shearing_col.png" alt="" />
          <img src="/assets/images/technologies/icons/slit_col.png" alt="" />
          <img src="/assets/images/technologies/icons/sheetmetal_col.png" alt="" />
        </div>
      </div>
    )
  }

}