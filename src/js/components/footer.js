import React, { Component } from "react"

export default class Footer extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount(){
  }

  render(){
    return (
      <footer>
        <div className="footer__container">
          <div className="upper">
            <div className="upper__contact">
              <div className="address">
                <div>
                  <a href="tel:0561382541">TEL 0561-38-2541</a>
                  <p>{this.props.lang === '/' ?
                    '受付時間：平日 8:30 - 17:15' :
                    'Business hours: 8:30 a.m. to 5:15 p.m. weekdays'}</p>
                </div>
                <div>
                  <a>FAX 0561-38-2545</a>
                  <p>{this.props.lang === '/' ?
                    '受付時間：平日 8:30 - 17:15' :
                    'Business hours: 8:30 a.m. to 5:15 p.m. weekdays'}</p>
                </div>
              </div>
              <div className="btn__contact">
                <a href="mailto:toiawase@mishina-matubishi.co.jp">CONTACT</a>
              </div>
            </div>
            <div className="company">
              <div className="logo"><img src="/assets/images/logo.png" alt="株式会社三品松菱"/></div>
              <p>{this.props.lang === '/' ?
                '〒470-0151　愛知県愛知郡東郷町大字諸輪字北山158番地' :
                'Kitayama 158, Morowa, Togo-cho, Aichi-gun, Aichi-ken, Japan 470-0151'}</p>
            </div>
          </div>
        </div>
        <div className="lower">
          <p>COPYRIGHT &copy; MISHINA MATSUBISHI CORPORATION ALL RIGHTS RESERVED.</p>
        </div>
      </footer>
    )
  }

}