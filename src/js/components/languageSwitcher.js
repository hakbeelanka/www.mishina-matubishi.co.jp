import React, { Component } from "react"

export default class LanguageSwitcher extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  render(){
    return (
      <div className="language_switcher">
        <div className="inner">
          <button onClick={e => {
            this.props.handler('/en/')
            window.location.pathname = '/en/'
          }}
                  className={this.props.lang === '/en/' ? "active" : null}
          >EN</button>
          <button onClick={e => {
            this.props.handler('/')
            window.location.pathname = '/'
          }}
                  className={this.props.lang === '/' ? "active" : null}
          >JP</button>
        </div>
      </div>
    )
  }

}