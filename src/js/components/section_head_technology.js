import React, { Component } from "react"
import ClassNames from "classnames"

export default class SectionHeadTechnology extends Component {

  constructor(props) {
    super(props)
    this.state = {
      icon: this.props.icon
    }
  }

  componentDidMount(){

  }

  render(){

    const headClass = ClassNames(
      "section_head_technology"
    )

    const Switcher = () => {
      return (
        <div className={`heading ${this.state.icon}`} />
      )
    }
    return (
      <section className={headClass}>
        {Switcher()}
      </section>
    )
  }

}

