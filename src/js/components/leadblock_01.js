import React, { Component } from "react"
import ClassNames from "classnames"

export default class LeadBlock01 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title,
      sub: this.props.sub ? this.props.sub : ".",
      theme: this.props.theme ? this.props.theme : null,
      render: this.props.render ? this.props.render : this.props.children
    }
  }

  render() {

    let leadClass = ClassNames(
      "leadblock01",
      {
        "theme-gray": this.state.theme === "gray",
        "theme-white": this.state.theme === "white",
        "theme-gold": this.state.theme === "gold",
        "theme-black": this.state.theme === "black",
        "theme-ja-black": this.state.theme === "ja-black",
        "theme-ja-gray": this.state.theme === "ja-gray"
      }
    )

    return (
      <div className={leadClass}>
        <div className="leadblock01__heading">
          <h2 data-sub={this.state.sub}>{this.state.title}</h2>
        </div>
        <div className="leadblock01__content">
          {this.state.render}
        </div>
      </div>
    )
  }

}