import React, { Component } from "react"
import ClassNames from "classnames"

export default class LeadBlock03 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title ? this.props.title : null,
      theme: this.props.theme ? this.props.theme : null,
      render: this.props.render ? this.props.render : this.props.children,
      last: this.props.last ? this.props.last : false
    }
  }

  render() {

    let leadClass = ClassNames(
      "leadblock03",
      {
        "theme-gold": this.state.theme === "gold",
        "last": this.state.last
      }
    )

    return (
      <div className={leadClass}>
        <div className="leadblock03__heading">
          {this.state.title ? <h2>{this.state.title}</h2> : null}
        </div>
        <div className="leadblock03__content">
          {this.state.render}
        </div>
      </div>
    )
  }

}