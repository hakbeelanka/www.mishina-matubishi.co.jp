import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"
import {Link} from "react-router-dom"
import Particles from 'react-particles-js';

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount(){　}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
    this.refs.mainVd.addEventListener('loadeddata', ()=>{
      this.refs.mainVd.play();
      this.refs.clipVd.play();
    })

  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    let particleNumber = (this.state.width > 860) ? 80: 20;
    
    let calculateSize;
    if (this.state.width > 860) {
      if (this.state.height / this.state.width < 1080/1920){
        calculateSize = `width: 100%`;
      } else {
        calculateSize = `height: 100%`;
      }
    } else {
      if (this.state.height > this.state.width){
        calculateSize = `width: 100%`;
      } else {
        calculateSize = `width: 100%`;
      }
    }

    let movieHeroSwitcher = () => {
      if (this.state.width > 860) {
        return (
          <div className={`bg_video`}>
            <video
              src="/assets/videos/mishina_30mb.mp4"
              ref="mainVd"
              loop={true}
              muted={true}
            />
            <div className="video__clip">
              <video
                ref="clipVd"
                src="/assets/videos/mishina_30mb.mp4"
                loop={true}
                muted={true}
              />
              <div className="clip__wrapper">
                <svg viewBox="0 0 1000 555" className="svg-overlay">
                  <defs>
                    <clipPath id="mask">
                      <rect className="cls-1" x="117.29" y="55.96" width="78.05" height="13.99"/>
                      <rect className="cls-1" x="117.29" y="79.53" width="78.05" height="13.99"/>
                      <rect className="cls-1" x="117.29" y="103.09" width="78.05" height="13.99"/>
                      <rect className="cls-1" x="263.82" y="77.6" width="48.61" height="19.31" rx="9.65" ry="9.65"/>
                      <path className="cls-1"
                            d="M250.2,120.87c-.54.45-1,.24-1-.47V54c0-.71.45-.92,1-.47l39.53,32.82a1,1,0,0,1,0,1.64Z"/>
                      <path className="cls-1"
                            d="M257.58,73S180,49.46,131.31.78c-2-2-5.1.1-3.11,2.88,1.09,1.51,54.88,65.82,132.56,85.92C272.57,86.05,257.58,73,257.58,73Z"/>
                      <path className="cls-1"
                            d="M257.58,99.49S180,123,131.31,171.69c-2,2-5.1-.1-3.11-2.88,1.09-1.51,54.88-65.83,132.56-85.92C272.57,86.42,257.58,99.49,257.58,99.49Z"/>
                      <rect className="cls-1" y="77.6" width="48.6" height="19.31" rx="9.65" ry="9.65"/>
                      <path className="cls-1"
                            d="M62.22,120.87c.54.45,1,.24,1-.47V54c0-.71-.45-.92-1-.47L22.7,86.39a1,1,0,0,0,0,1.64Z"/>
                      <path className="cls-1"
                            d="M54.84,73S132.42,49.46,181.11.78c2-2,5.11.1,3.1,2.88-1.07,1.51-54.86,65.82-132.54,85.92C39.85,86.05,54.84,73,54.84,73Z"/>
                      <path className="cls-1"
                            d="M54.84,99.49s77.58,23.53,126.27,72.2c2,2,5.11-.1,3.1-2.88C183.14,167.3,129.35,103,51.67,82.89,39.85,86.42,54.84,99.49,54.84,99.49Z"/>
                    </clipPath>
                  </defs>
                </svg>
              </div>
            </div>
          </div>
        )
        // this.refs.mainVd.play();
        // this.refs.clipVd.play();
      } else {
        return (
          <div className={`bg_video`}>
            <video
              src="/assets/videos/mishina_30mb.mov"
              ref="mainVd"
              loop={true}
              muted={true}
              autoPlay={true}
              playsInline={true}
            />
          </div>
        )
        // this.refs.mainVd.play();
      }
    }

    let movieEarthSwitcher = () => {
      if (this.state.width > 860) {
        return (
          <video src="/assets/videos/earth.mp4" autoPlay={true} muted loop />
        )
      } else {
        return (
          <img src="/assets/videos/earth.gif" alt="earth" />
        )
      }
    }

    return Style.it(`
          .page__container .home_lead {
            height: ${this.state.height}px;
          }
          .page__container:before {
            border-right: ${this.state.width}px solid transparent;
            bottom: ${this.state.width * 0.74}px;
          }
          #mask {
            transform: translate(${this.state.width/2 - 937/2}px,${this.state.height/2 - 518/2}px) scale(3);
          }
          video {
            ${calculateSize}
          }
          `,
      <div className="page home">
        <section className={`hero`} style={{height: `${this.state.height}px`}}>
          {movieHeroSwitcher()}
          <div className="copy">
            {/*<h1>BE ESSENTIAL<span>WITH</span>IRON HEART</h1>*/}
            {/*<h2>常に本質的であれ、鉄の意志とともに</h2>*/}
            <h1><img src="/assets/images/top/title.png" alt="KANTETSU ~Beyond steel~"/></h1>
          </div>
          <button className={`btn__scroll`}>SCROLL</button>
        </section>
        <section>
          <div className="heroblock01 is-phlosophy" style={{backgroundColor: 'rgba(255,255,255,0.83)'}}>
            <div data-index="01" className="heroblock01__heading">
              <h2>PHILOSOPHY</h2>
              <h3>私たちの鉄則</h3>
            </div>
            <div className="heroblock01__lead">
              <h1>鉄を貫く、という覚悟</h1>
              <p>三品松菱は、創業80余年。<br />
                古くから「産業のコメ」と重用された<br />
                鉄にこだわり、鉄を究め抜いた私たちには<br />
                総合鋼材加工メーカーとしての<br />
                揺るぎない哲学があります。
              </p>
              <div className="btn__more">
                <Link to="/philosophy">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01" style={{backgroundColor: 'rgb(0,0,0)'}}>
            {movieEarthSwitcher()}
            <Particles
              params={{
                particles: {
                  number: {
                    value: particleNumber
                  }
                }
              }}
            />
            <div data-index="02" className="heroblock01__heading">
              <h2>VISION</h2>
              <h3>未来への展望</h3>
            </div>
            <div className="heroblock01__lead is-right">
              <h1>もっと、金属は<br />ユニークになる</h1>
              <p>美しい光沢をもつ高精度な磨き帯鋼を<br />
                小ロットで供給する三品松菱。<br />
                鉄鋼産業のなかでもひときわユニークな<br />
                存在である私たちの未来は、<br />
                想像力と創造力に溢れています。
              </p>
              <div className="btn__more">
                <Link to="/vision">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01 is-technology" style={{
            backgroundImage: `url(${'/assets/images/top/bg_03.png'})`
          }}>
            <div data-index="03" className="heroblock01__heading">
              <h2>TECHNOLOGY</h2>
              <h3>技術紹介</h3>
            </div>
            <div className="heroblock01__lead is-right">
              <h1 className="is-adjust">「最適化」をかなえる<br className={`only-pc`} />技術力</h1>
              <p>たとえば、もっと強い鋼材がほしい。<br />
                厚みの違う鋼材が数種類ほしい。<br />
                お客さまに最適な金属組織を創り上げる、<br />
                それが三品松菱の誇るコア技術。<br />
                磨き上げた職人技で、最適化を図ります。
              </p>
              <div className="btn__more">
                <Link to="/technologies">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01" style={{backgroundImage: `url(${'/assets/images/top/bg_04.png'})`}}>
            <div data-index="04" className="heroblock01__heading">
              <h2>PRODUCTS</h2>
              <h3>製品について</h3>
            </div>
            <div className="heroblock01__lead">
              <h1>お客さまのために<br />私たちができること</h1>
              <p>三品松菱がお届けするのは<br className={`only-sp`} />
                単なる鋼材ではありません。<br />
                お客さまの課題を解決し、<br className={`only-sp`} />
                新たな可能性を拓く提案力こそが<br />
                製品を通じて提供する「価値」です。
              </p>
              <div className="btn__more">
                <Link to="/products">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01 is-history" style={{backgroundImage: `url(${'/assets/images/top/bg_05.png'})`}}>
            <div data-index="05" className="heroblock01__heading is-white">
              <h2>HISTORY</h2>
              <h3>三品松菱のあゆみ</h3>
            </div>
            <div className="heroblock01__lead">
              <img src="/assets/images/top/history.png" alt="history"/>
              <h1>匠の技を<br />極めつづけた歴史</h1>
              <p>
                太平洋戦争勃発7年前の1934年。<br />
                三品松菱の歴史はそこからはじまりました。<br />
                航空機、自転車、バイクにクルマ<br />
                産業の発展に貢献するために<br />
                職人技を磨きつづけた歴史を紹介します。
              </p>
              <div className="btn__more">
                <Link to="/history">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section className={`flex-row`}>
          <div className="heroblock02" style={{backgroundImage: `url(${'/assets/images/top/bg_06.png'})`}}>
            <Link to="/about">
              <div data-index="06" className="heroblock02__heading">
                <h2>ABOUT US</h2>
                <h3>会社概要</h3>
              </div>
            </Link>
          </div>
          <div className="heroblock02" style={{backgroundImage: `url(${'/assets/images/top/bg_07.png'})`}}>
            <Link to="/recruit">
              <div data-index="07" className="heroblock02__heading">
                <h2>RECRUIT</h2>
                <h3>採用情報</h3>
              </div>
            </Link>
          </div>
        </section>

        <section>
          <div className="heroblock01 is-future" style={{backgroundColor: '#e1e2e5'}}>
            <img src="/assets/images/top/future.png" alt="Future"/>
            <div data-index="08" className="heroblock01__heading">
              <h2>FUTURE</h2>
              <h3>発動！未来創造プロジェクト</h3>
            </div>
            <div className="heroblock01__lead">
              <p>
                もう、はじまっている。<br />
                私たちの「未来を叶える」挑戦。<br />
                とびきりユニークなアイデアが<br />
                新たな歴史を刻むために、<br />
                動き出しています。
              </p>
              <div className="btn__more is-blank">
                <a href="https://www.mishina-and-co.com/miraisouzou/" target={`_blank`}>READ MORE</a>
              </div>
            </div>
          </div>
        </section>

      </div>
    );
  }
}
