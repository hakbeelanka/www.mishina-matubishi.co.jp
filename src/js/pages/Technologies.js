import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../components/section_head"
import SectionNext from "../components/section_next"
import LeadBlock01 from "../components/leadblock_01"
import TechnologyList from "../components/technologyList"

const TECHNOLOGY_QUALITY = (
  <ul className={`technology_quality`}>
    <li>
      <h3>ISO9001・14001取得</h3>
      <p>2002年、三品松菱では、ISO9001およびISO14001を取得しました。地域とともにある企業として、品質の追求と地球環境への負荷低減は、両立する責任がある。その信念が、2つのISOの同時取得となりました。</p>
    </li>
    <li>
      <h3>品質・環境への取り組み</h3>
      <p>製造工程では、全ての製品においてサンプルを抜き取り、厳密な検査を行う管理体制を構築しています。また、コイルの全長にわたって厚みのデータを収集し、全長品質保証を行うことも可能です。</p>
    </li>
    <li>
      <h3>ITシステムによるトレーザビリティの実現</h3>
      <p>鉄鋼メーカーから購入した鋼板・コイルが、各工程で加工され出荷に至るまでの履歴を、社内ITシステムで一元管理。万が一、納品後に不具合が発見された場合は、原材料まで遡った徹底的な原因調査が可能です。</p>
    </li>
  </ul>
)

export default class Technologies extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHead
          poster={`/assets/images/technologies/h1img.png`}
          title={`TECHNOLOGY`}
          sub={`技術紹介`}
          theme={`white`}
        />
        <section>
          <div className={`heroblock04`}>
            <h1>鋼を制する技術を磨き、<br/>
              お客さまに最適な製品を。
            </h1>
            <p>
              三品松菱が誇る技術の真髄は、<br/>
              望みどおりの金属組織を創りあげることにあります。<br/>
              用途に応じて最適な組織状態となるように<br/>
              製品ごとに工程を設計し、加工。<br/>
              必要な数量の製品を、ご要望どおりの寸法で<br/>
              お客さまが使いやすいように、美しく仕上げます。
            </p>
          </div>
        </section>
        <section>
          <LeadBlock01
            title={`MANUFACTURING PROCESS`}
            sub={`最適化を実現する製造工程`}
            theme={`black`}
            render={TECHNOLOGY_PROCESS}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
        <section>
          <LeadBlock01
            title={`QUALITY CONTROL AND ASSURANCE SYSTEM`}
            sub={`品質管理・保証体制`}
            theme={`white`}
            render={TECHNOLOGY_QUALITY}
          />
        </section>
        <SectionNext
          current={`technologies`}
          nextid={`04`}
          nextPath={`/products`}
          title={`PRODUCTS`}
          sub={`製品について`}
        />
      </div>
    );
  }
}

const TECHNOLOGY_PROCESS = (
  <div className={`technology_process`}>
    <ul className={`factory`}>
      <li>
        <h3>本社工場</h3>
        <p>本社工場では、「超高精度・高付加価値製品」を中心に生産しています。</p>
        <picture className={`process_image`}>
          <source srcSet="/assets/images/technologies/process_honsha.png"
                  media="(min-width: 960px)" />
          <img src="/assets/images/technologies/process_honsha-sp.png" alt="car"/>
        </picture>
        <div className={`factory_photos`}>
          <picture>
            <img src="/assets/images/technologies/photo01_honsha.png" alt="本社工場" />
          </picture>
          <picture>
            <img src="/assets/images/technologies/photo02_honsha.png" alt="本社工場" />
          </picture>
        </div>
        <div className={`links`}>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_TOGO.pdf">本社工場 設備仕様一覧<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
        </div>
      </li>
      <li>
        <h3>豊田工場・タイ工場</h3>
        <p>豊田工場とタイ工場では、工業品・大量生産品の加工を中心に行っています。</p>
        <picture className={`process_image`}>
          <source srcSet="/assets/images/technologies/process_other.png"
                  media="(min-width: 960px)" />
          <img src="/assets/images/technologies/process_other-sp.png" alt="car"/>
        </picture>
        <div className={`factory_photos`}>
          <picture>
            <img src="/assets/images/technologies/photo01_other.png" alt="豊田工場・タイ工場" />
          </picture>
          <picture>
            <img src="/assets/images/technologies/photo02_other.png" alt="豊田工場・タイ工場" />
          </picture>
        </div>
        <div className={`links`}>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_TOYOTA01.pdf">豊田工場第1 設備仕様一覧<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_TOYOTA02.pdf">豊田工場第2 設備仕様一覧<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_Thailand.pdf">タイ工場 設備仕様一覧<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
)
