import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../components/section_head"
import SectionNext from "../components/section_next"

export default class Vision extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount(){　}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page vision">
        <SectionHead
          poster={`/assets/images/vision/h1img.png`}
          title={`VISION`}
          sub={`未来への展望`}
          theme={`white`}
        />
        <section>
          <div className={`deco1`} />
          <div className={`deco2`} />
          <div className={`heroblock04`}>
            <h1>私たちが夢を叶えられる理由。</h1>
            <p>
              ミクロン精度のハイスペックな1点ものの生産も<br />
              高品質な大量産品の加工も同時に手がける。<br />
              あくまで鉄に軸足をおきつつ<br />
              さらなる可能性を探り、新事業にも挑戦する。<br />
              私たちがそれを貫鐡できるのは、<br />
              今まで培ってきた技術力に対する誇りと<br />
              世界産業の発展に貢献するという目標があるから。<br />
              今や全国でも片手で数えられるほど希少な存在である<br />
              鉄を知り尽くした圧延加工の専門メーカーとして、<br />
              三品松菱にしか描けない夢を叶えていきます。
            </p>
          </div>
          <div className={`heroblock05 vision-01`}>
            <div className={`heroblock05__heading is-index`} data-index={`01`}>
              <h2 data-sub={`FLEXIBLE`}>高炉メーカーと同じ設備を<br />よりフレキシブルに稼働できる。</h2>
            </div>
            <div className={`heroblock05__content`}>
              <p>三品松菱が高精度の鋼材を少量で生産可能なのは、なぜでしょうか。その理由は、高炉をもつ大手鉄鋼メーカーと同じ工程をもち、同様ながらもより小規模な設備が揃っていることにあります。圧延・焼鈍・酸洗・レベラーカット・シャーリング・スリットという工程をすべて揃え、鋼板とコイル両方の圧延加工ができる企業は全国でもわずか数社ほど。特に表面が均一で美しい光沢をもつ磨き材に必要な冷間圧延加工を、鋼板とコイル両方でできることは私たちの大きな強みとなっています。匠の技をもつ熟練職人によって、多様な加工を小規模な設備で行うことから、鋼板1枚・コイル1本の少量生産が可能なのです。</p>
            </div>
          </div>
          <div className={`heroblock05 is-white vision-02`}>
            <div className={`heroblock05__heading`} data-index={`02`}>
              <h2 data-sub={`MATERIAL`}>美しく使い勝手のよい鉄へ。<br />「改質」で素材の可能性を広げる。</h2>
            </div>
            <div className={`heroblock05__content`}>
              <p>例えば自動車の開発段階における試作品用など、三品松菱には極めて難易度の高い特注品の依頼も多く寄せられます。それに応えるコア技術が「改質」。鋼材はどの厚さから何度圧延し、どんな温度で何時間熱処理を行うかによって、金属組織の状態がまったく変わります。組織まで自在に操るノウハウこそ、長年培ってきた財産。私たちは改質技術をさらに磨き、より薄い鋼板の製造や特殊鋼の加工に取り組み、クラッド材製造にも対応可能な圧延技術、表面処理技術やブランキングプレスによる型抜き加工などの周辺技術についても確立を目指しています。さらに、新しい機能材料の研究開発と製造にも挑戦。将来の部品製造を見据えた技術研究も進めています。</p>
            </div>
          </div>
          <div className={`heroblock05 vision-03`}>
            <div className={`heroblock05__heading`} data-index={`03`}>
              <h2 data-sub={`INTERNATIONAL`}>国内では、高付加価値の鉄加工。<br />海外では、日本品質の量産製品を。</h2>
            </div>
            <div className={`heroblock05__content`}>
              <p>メイド・イン・ジャパンの高品質な製品を、世界の市場へ。<br className="only-pc"/>三品松菱は2011年6月に、タイの首都バンコクに駐在員事務所を開設、2012年1月にはバンコク近郊の地方都市チョンブリに現地法人「MISHINA MATSUBISHI (THAILAND)」を設立しました。大量生産品の加工を行うタイ工場では、冷間圧延による厚さ0.1mmの極薄鋼材も生産可能。タイを拠点に中国やインドネシア、マレーシア、ベトナムなどのアジア地域への販売を目指すとともに、北米など世界への供給体制を構築します。世界を舞台に展開する一方で、国内では本社工場を中心とした「超高精度・高付加価値製品」<br className="only-sp"/>の生産に注力。国内と海外、異なる戦略のもと、<br className="only-sp"/>地球規模の産業発展に貢献していきます。</p>
            </div>
          </div>
          <div className={`heroblock05 vision-04`}>
            <div className={`heroblock05__heading `} data-index={`04`}>
              <h2 className={`is-white`} data-sub={`CHALLENGE`}>鉄を究めた先にある、<br />100年後を見据えた挑戦。</h2>
            </div>
            <div className={`heroblock05__content is-white`}>
              <p>私たちが今の技術を手にするまでに、<br />
                80余年という長い時間がかかりました。<br />
                理想の実現に100年かかるのなら、今こそ<br />
                100年後の未来に向けた挑戦をはじめるときです。<br />
                <br />
                これまでに培ってきた技術力のすべてを懸けて<br />
                新しい夢を描く。鉄と人とを、もっと近づける。<br />
                それはとてもユニークで、クリエイティブな挑戦です。</p>
            </div>
          </div>
          <div className={`heroblock06`}>
            <ul>
              <li data-sub="新たな挑戦1">
                <h3>「化学」の視点で金属の力を引き出す。</h3>
                <p>工程の一つである「酸洗」に化学の力を必要とすることから、私たちは化学メーカーとしての一面ももっています。そうした化学処理の技術を応用して開発した製品が「還元鉄」や「キレート鉄」。金属としての性質を保ちながら、加工も形状も自由度がぐんと高まることで、多彩な用途への使用が可能です。最初に手がけるのは、ミネラル成分を活用した農業用肥料の製品化。それを皮切りに、バイオ事業の展開を図ります。</p>
              </li>
              <li data-sub="新たな挑戦2">
                <h3>新素材と技術の開発を進めて、空を飛ぶ。</h3>
                <p>コア技術の深化とともに進めている、新素材や部品製造技術の開発。これらの技術を活用して、ドローンや飛行船などのエアロ事業を展開します。航空関連事業は、まさに三品松菱の原点。初志を貫鐡し、「医・食・住」分野へ参入することで、人と鉄の新しい未来を拓きます。</p>
              </li>
            </ul>
          </div>
        </section>
        <SectionNext
          current={`vision`}
          nextid={`03`}
          nextPath={`/technologies`}
          title={`TECHNOLOGY`}
          sub={`技術紹介`}
        />
      </div>
    );
  }
}
