import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import {factories} from "../data/factories.json"
import {company_info} from "../data/company_info.json"

import SectionHead from "../components/section_head"
import SectionNext from "../components/section_next"
import LeadBlock01 from "../components/leadblock_01"
import ListBlock01 from "../components/listblock01"

const COMPANY_INFO = (
  <div className={`company_info`}>
    <ListBlock01
      data={company_info}
      theme={`white`}
    />
  </div>
);

const FACTORIES = () => (
  <ul className="factories">{
      factories.map((factory, i) => {
        return (
          <li key={i}>
            <div className="info">
              <h3>{factory.name}</h3>
              <p className="address" data-postalcode={factory.postalcode}>{factory.address}<a target="_blank" rel="noreferrer noopener" href={`${factory.latlng}`}>google map</a></p>
              <p>TEL {factory.tel}</p>
              <p>FAX {factory.fax}</p>
            </div>
            <div className="facade">
              <img src={`/assets/images/about/factories/${factory.facade}`} alt={factory.name}/>
            </div>
          </li>
        )
      })
  }</ul>
)



export default class About extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page about">
        <SectionHead
          poster={`/assets/images/about/h1img.png`}
          title={`ABOUT US`}
          sub={`会社概要`}
          theme={`white`}
        />
        <section>
          <LeadBlock01
            title={`COMPANY INFO`}
            sub={`会社概要`}
            theme={`gray`}
            render={COMPANY_INFO}
          />
        </section>
        <section>
          <LeadBlock01
            title={`PLANTS`}
            sub={`拠点案内`}
            theme={`white`}
            render={FACTORIES()}
          />
        </section>
        <SectionNext
          current={`about`}
          nextid={`07`}
          nextPath={`/recruit`}
          title={`RECRUIT`}
          sub={`採用情報`}
        />
      </div>
    );
  }
}
