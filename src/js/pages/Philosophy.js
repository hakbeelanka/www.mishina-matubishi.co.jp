import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../components/section_head"
import SectionNext from "../components/section_next"

export default class Philosophy extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount(){　}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page philosophy">
        <SectionHead
          poster={`/assets/images/philosophy/h1img.png`}
          title={`PHILOSOPHY`}
          sub={`私たちの鉄則`}
          theme={`black`}
        />
        <section className={`keyword`}>
          <div className={`keyword__wrapper`}>
            <img src="/assets/images/philosophy/kantetsu.png" alt="貫鐡"/>
            <h3>KANTETSU</h3>
            <h2>脈々と息づく、創業以来のDNA</h2>
          </div>
        </section>
        <section>
          <div className="columnblock01">
            <figure><img src="/assets/images/philosophy/photo01.png" alt="それは、鉄の意志を継承するパイオニア集団の原理原則" /></figure>
            <div className={`columnblock01__wrapper`}>
              <h3>それは、鉄の意志を継承する<br />パイオニア集団の原理原則</h3>
              <p>
                代々、鉄鋼業を営む一家の二男として生まれた創業者の三品實は、パイオニア精神に溢れた職人気質の技術者。加工機械の設計や開発を自ら手がけ、誤差わずか±0.02mmという高い精度を実現する敏腕な職人であるだけでなく、社員たちに熱心な技術指導を行い、企業としての競争力を高めていった優秀な経営者でもありました。鉄を極めつづけて80余年、創業者から伝承された技術と信念は今や私たちの財産となり、さらなる挑戦の原動力となっています。お客さまのために、世界の産業発展のために。「誠実」「忍耐」「努力」をモットーに、新たな発展へと初志貫徹する。そんな鉄の意志こそ「貫鐡」、いつの時代も鉄と向き合い続ける三品松菱の哲学です。</p>
            </div>
          </div>
          <div className="columnblock01">
            <figure><img src="/assets/images/philosophy/photo02.png" alt="時代の先をゆく発想で技術力の飛躍を叶えてきた。"/></figure>
            <div className={`columnblock01__wrapper`}>
              <h3>時代の先をゆく発想で<br />技術力の飛躍を叶えてきた。</h3>
              <p>
                1934年に創業した三品松菱は、当初航空機関連の鋼板圧延加工を手がけていました。なかでも高度な技術を必要としたのは、零戦のパイロットがかぶるヘルメット。当時のヘルメットは鉄製で、安全性を保つためには均等な厚みと強度、加工のしやすさが必須でした。それらを叶える技術に絶大な信頼を得て、終戦後はバイクやクルマの部品に用いる鋼板・鋼材の加工を手がけるようになります。そして1961年には、当時の常識だった平ベルトの代わりにVベルトを駆動に用いたユニークな「冷間二段圧延機」が完成。一歩進んだ発想が、他社の追随を許さない技術力の飛躍につながりました。今も現役で活躍するこの機械は、鉄を貫き、鉄で勝負する覚悟の象徴です。</p>
            </div>
          </div>
        </section>
        <section>
          <div className="heroblock03" style={{backgroundImage: `url("/assets/images/philosophy/bg_topmessage.png")`}}>
            <div className="container">
              <h2>MESSAGE FROM OUR TOP MANAGEMENT</h2>
              <h3>鉄の可能性を、<br />未来の発展に<br className={`sp-only`} />つなげる。</h3>
              <b><span>代表取締役社長</span><br />三品 安史</b>
              <p>
                私たち三品松菱は、鉄鋼メーカーが生産した鉄を、お客さまにとって最適な状態に加工してお届けする企業です。創業以来のDNAを受け継ぐ職人による高精度な鋼板・鋼帯材料を、ニーズに合わせて小ロットで供給。単なる「材料」ではなく、お客さまの課題を材料の最適化によって解決する「ソリューション」がいわば私たちの商品です。100年後の未来を想像すれば、大量生産型の生産方式は終焉を迎え、「数の力」に依存しないフレキシブルな生産方式への変革が起きていくことでしょう。私たちはこの変革を先導すべく、究極には「一品づくり」生産にも対応した、金属材料の柔軟な供給を行っていきます。鉄を究め続けた先にある未来の夢、その実現は必ず世界産業の発展に貢献する。そんな確信のもと、独自商品や新生産技術の開発に努め、世界各国への供給体制の構築に向けて、これからも「貫鐡」していきます。</p>
            </div>
          </div>
        </section>
        <SectionNext
          current={`philosophy`}
          nextid={`02`}
          title={`VISION`}
          sub={`未来への展望`}
          nextPath={`/vision`}
        />
      </div>
    );
  }
}
