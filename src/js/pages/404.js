import React, { Component } from "react";
import $ from "jquery";

export default class Projects extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

	render() {
		return (
			<div className="page missed" style={{height: this.state.height}}>
				<div className="container">
          <h1>COMING SOON</h1>
          {/*<p>ページが見つかりません</p>*/}
				</div>
			</div>
		);
	}
}
