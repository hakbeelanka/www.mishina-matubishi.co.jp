import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../components/section_head"
import SectionNext from "../components/section_next"
import LeadBlock01 from "../components/leadblock_01"

const PRODUCTS_TYPE = (
  <div>
    <ul className={`products_type`}>
      <li><h3 data-num="①">普通鋼</h3>
        <p>SPHC/SPHD/SPHE/SAPH/SS400 など</p>
      </li>
      <li><h3 data-num="②">特殊鋼</h3>
        <p>SAE/SC/SK/SCr/SCM など</p>
      </li>
      <li><h3 data-num="③">磨き帯鋼</h3>
        <p>SPCC/SPCD/SPCE/SAPH/SS400/SAE/SC/SK/SCr/SCM など</p>
      </li>
      <li><h3 data-num="④">表面処理材</h3>
        <p>SGHC/SGAHC/ZAM など</p>
      </li>
      <li><h3 data-num="⑤">ステンレス鋼</h3>
        <p>SUS304/SUS409/ZAM など</p>
      </li>
      <li><h3 data-num="⑥">非鉄金属</h3>
        <p>銅/アルミ/チタン など</p>
      </li>
    </ul>
    <p className="products_type_caption">掲載していない鋼種でもお問い合わせ下さい。</p>
  </div>
)

const PRODUCTS_SOLUTION = (
  <ul className={`products_solution`}>
    <li><h3>欲しい厚さの鋼板が見つからない</h3>
      <p>圧延により、ご希望の厚さの鋼板・鋼帯の製造が可能です。</p>
    </li>
    <li><h3>小ロットの納品を希望している</h3>
      <p>鋼板大板1枚、スリットコイル1C（幅50mm以上）から対応いたします。</p>
    </li>
    <li><h3>短納期でほしい</h3>
      <p>おまかせください。小回りを効かせ、短納期にも対応いたします。</p>
    </li>
    <li><h3>希望する品質の鋼板や鋼帯が見つからない</h3>
      <p>ご希望の規格に対応可能な工程を組み、硬度や表面粗度、板厚公差などを調整いたします。</p>
    </li>
    <li><h3>在庫品から再加工してほしい</h3>
      <p>長期在庫品からの再加工もOK。冷間圧延、焼鈍などにて対応可能です。</p>
    </li>
    <li><h3>表面のきれいな材料がほしい</h3>
      <p>ブライトロールで仕上げていますので、製品の表面は平滑で光沢があります<br />
        例）Ra 0.4以下<br />
        照明付きミラーを設置するなど、キズ防止策を徹底。<br />
        画像処理による自動検出にも取り組んでいます。
      </p>
    </li>
    <li><h3>加工性に優れた材料を探している</h3>
      <div>
        <picture>
          <img src="/assets/images/products/solution02.png" alt="90%以上の球状化率を誇る焼鈍技術によりプレス加工、曲げ加工が可能。"/>
        </picture>
        <picture>
          <img src="/assets/images/products/solution01.png" alt="90%以上の球状化率を誇る焼鈍技術によりプレス加工、曲げ加工が可能。"/>
        </picture>
      </div>
      <p>90%以上の球状化率を誇る焼鈍技術によりプレス加工、曲げ加工が可能。<br />
        三品松菱が長年の経験の中で編み出してきた、温度条件による無酸化光輝焼鈍を行なっています。<br />
        焼鈍前（左）の金属組織の中に見える黒い固まりが、炭化物（セメンタイト）。<br />
        圧延と焼鈍とにより、この固まりが細かく砕かれて一様に球状化（右）します。
      </p>
    </li>
  </ul>
)

const PRODUCTS_AVAILABLE = (
  <div className={`products_available`}>
    <ul className={`type`}>
      <li>特殊鋼＋普通鋼範囲</li>
      <li>特殊鋼範囲</li>
      <li>製造能力拡大範囲</li>
      <li>受注検討範囲</li>
    </ul>
    <ul className={`area`}>
      <li>
        <h3>みがき鋼板範囲（シート）</h3>
        <picture>
          <img src="/assets/images/products/available01.png" alt="みがき鋼板範囲（シート）"/>
        </picture>
      </li>
      <li>
        <h3>みがき鋼板範囲（コイル）</h3>
        <picture>
          <img src="/assets/images/products/available02.png" alt="みがき鋼板範囲（コイル）"/>
        </picture>
      </li>
      <li>
        <h3>レベラーカット加工範囲</h3>
        <picture>
          <img src="/assets/images/products/available03.png" alt="レベラーカット加工範囲"/>
        </picture>
      </li>
      <li>
        <h3>酸洗処理範囲</h3>
        <picture>
          <img src="/assets/images/products/available04.png" alt="酸洗処理範囲"/>
        </picture>
      </li>
    </ul>
  </div>
)
export default class History extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page products">
        <SectionHead
          poster={`/assets/images/products/h1img.png`}
          title={`PRODUCTS`}
          sub={`製品について`}
          theme={`white`}
        />
        <section className={`products_main`}>
          <h2>
            どんな悩みもご相談ください。<br />
            その答えが、私たちの「製品」です。
          </h2>
        </section>
        <section>
          <LeadBlock01
            title={`私たちの加工した素材はこのような場所で使われております。`}
            theme={`ja-black`}
            render={(
              <div className={`products_use`}>
                <picture>
                  <source srcSet="/assets/images/products/car.png"
                          media="(min-width: 960px)" />
                  <img src="/assets/images/products/car_sp.png" alt="car"/>
                </picture>
              </div>
            )}
          />
        </section>
        <section>
          <LeadBlock01
            title={`こんなお悩み、ございませんか？`}
            theme={`ja-gray`}
            render={PRODUCTS_SOLUTION}
          />
        </section>
        <section>
          <LeadBlock01
            title={`製造可能範囲`}
            theme={`ja-black`}
            render={PRODUCTS_AVAILABLE}
          />
        </section>
        <section>
          <LeadBlock01
            title={`STEEL TYPE`}
            sub={`取り扱い鋼種`}
            render={PRODUCTS_TYPE}
          />
        </section>
        <SectionNext
          current={`products`}
          nextid={`05`}
          nextPath={`/history`}
          title={`HISTORY`}
          sub={`三品松菱のあゆみ`}
        />
      </div>
    );
  }
}
