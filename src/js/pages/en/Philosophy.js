import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../../components/section_head"
import SectionNext from "../../components/section_next"

export default class Philosophy extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount(){}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page philosophy lang-en">
        <SectionHead
          poster={`/assets/images/philosophy/h1img.png`}
          title={`PHILOSOPHY`}
          sub={`Our Unbending Principles`}
          theme={`black`}
        />
        <section className={`keyword`}>
          <div className={`keyword__wrapper`}>
            <img src="/assets/images/philosophy/kantetsu.png" alt="KANTETSU"/>
            <h3>KANTETSU</h3>
            <h2>The lifeblood of Mishina Matsubishi continues to<br />course through us from our founding.</h2>
          </div>
        </section>
        <section>
          <div className="columnblock01">
            <figure><img src="/assets/images/philosophy/photo01.png" alt="As a group of pioneers, our philosophy is to carry on with intention forged from steel." /></figure>
            <div className={`columnblock01__wrapper`}>
              <h3>As a group of pioneers, our philosophy is to "carry on with intention forged from steel.</h3>
              <p>
                Born as the second son in a family involved in the steel industry, company founder Minoru Mishina was a technician with the temperament of a craftworker and the spirit of a pioneer. Mishina was personally involved in the design and development of processing machinery. He was a talented businessman but also a highly competent craftsman who was able to achieve high precision with a tolerance of only ±0.02 mm, and gave enthusiastic technical guidance to the company's employees to strengthen the competitiveness of the company. The technical skill and beliefs we have maintained since the founding of Mishina Matsubishi in honing our craft with steel remain an asset to us, providing the impetus for us to meet new challenges. For our customers-for the growth of industry around the world. Our motto is "sincerity, endurance and effort," which we carry forward to grow in new ways while still making good on our original aims. The word we use to describe our intentions forged from steel is "kantetsu (carry out)," which is Mishina Matsubishi's philosophy of working with steel throughout the generations.</p>
            </div>
          </div>
          <div className="columnblock01">
            <figure><img src="/assets/images/philosophy/photo02.png" alt="With our era-leading ideas, we make leaps and bounds in technology a reality."/></figure>
            <div className={`columnblock01__wrapper`}>
              <h3>With our era-leading ideas, we make leaps and bounds in technology a reality.</h3>
              <p>
                Founded in 1934, Mishina Matsubishi was involved in rerolling steel plates for aircraft. At that time, advanced technology was required to manufacture the helmets used by Zero fighter pilots. The helmets of that time were made of steel, and the metal used to manufacture them needed to be uniform in thickness and strength for easy processing, in order to provide safety for the wearer. Mishina Matsubishi earned a massive level of trust in our ability to make this technology a reality; and after the war, our company became involved in processing steel plates and materials for motorcycles and cars. In 1961, we completed a unique machine called the "two-high cold rerolling mill," which used a V-belt drive rather than the flat-belt drive that was considered standard at the time. Our ideas are a step ahead of the times, and these ideas have led to leaps in technical skill that outstrip our competitors. The two-high cold rerolling mill is still actively used, and symbolizes our unwavering resolve that pierces steel-the stage on which we compete.</p>
            </div>
          </div>
        </section>
        <section>
          <div className="heroblock03" style={{backgroundImage: `url("/assets/images/philosophy/bg_topmessage.png")`}}>
            <div className="container">
              <h2>MESSAGE FROM OUR TOP MANAGEMENT</h2>
              <h3>We link the possibilities of steel to our expansion towards the future.</h3>
              <b><span>Chief Executive Officer</span><br />Yasuhito Mishina</b>
              <p>
                At Mishina Matsubishi, we are committed to bringing the steel produced by steel manufacturing companies to our customers, by processing them into the forms that our customers can optimally use. Carrying on the lifeblood of our company since our founding, as craftworkers we continue to supply high-precision steel plates and strips in small lots to meet the needs of our customers. The products we provide are not simply "materials," but solutions that offer the optimum material to solve each customer's needs. Looking 100 years ahead, we can see that mass-production systems are going to fall out of use, and that production systems will need to be reinvented to be more flexible, so as not to rely upon quantity. As the leaders of this revolution, we at Mishina Matsubishi will continue to flexibly supply metallic materials, through ultimately offering "single product production" as well. Our unshakeable aim is to contribute to the growth of industry around the world, brought about by realizing our dreams for the future: continuing to take steel to its limits. With this confidence, we strive towards developing unique products and new production technologies, and continue pushing ahead with our "kantetsu (carry out)" philosophy as we create a supply system to serve every country of the world.</p>
            </div>
          </div>
        </section>
        <SectionNext
          current={`philosophy`}
          nextid={`02`}
          title={`VISION`}
          nextPath={`/en/vision`}
        />
      </div>
    );
  }
}
