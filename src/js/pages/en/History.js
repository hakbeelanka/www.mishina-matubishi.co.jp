import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import {company_history} from "../../data/en/company_history.json"

import SectionHead from "../../components/section_head"
import SectionNext from "../../components/section_next"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import ListBlock01 from "../../components/listblock01"
import ColumnBlock02 from "../../components/columnblock02"

const COMPANY_HISTORY = (
  <div className={`company_history`}>
    <ListBlock01
      data={company_history}
      theme={`white`}
    />
  </div>
);

const HISTORY_TITLE = (
  <span>
    The Cornerstone of our Technology and Pride:<br />Our Unique Machines, Still In Use Today.
  </span>
)

const HISTRY_COLUMNS = (
  <section>
    <ColumnBlock02
      wrap01={(
        <figure>
          <img src="/assets/images/history/photo01.png" alt="Outside of plant in Atsuta Ward, around the time of the company's founding"/>
          <figcaption>Outside of plant in Atsuta Ward, around the time of the company's founding</figcaption>
        </figure>
        )}
      wrap02={(
        <p>Mishina Matsubishi was founded in Atsuta Ward in the city of Nagoya, around the area where Kanayama station now stands.<br/>
        Although the company began with rolling steel plates for aviation-related purposes, our technology evolved by leaps and bounds with the creation of our "two-high cold rerolling mill" in 1961, designed and developed by the company's founder, Minoru Mishina. This machine was unique in its time, in that it used a V-belt drive rather than the flat-belt drive that was considered standard at the time. Owing to this, the mill could be driven in a stable way, even when the supply of electricity was less than favorable. The machine became the talk of the town amongst fellow industrial manufacturers in the Kansai region.</p>
      )}
      reverse={false}
    />
    <ColumnBlock02
      wrap01={(
        <figure>
          <img src="/assets/images/history/photo02.png" alt="Introduction of annealing furnace established our three pillars: pickling, annealing and rerolling"/>
          <figcaption>Introduction of annealing furnace established our three pillars: pickling, annealing and rerolling</figcaption>
        </figure>
      )}
      wrap02={(
        <p>As the developer of the machine and a craftworker unparalleled in skill, Minoru Mishina operated the mill with a fine touch, and was able to achieve a minute tolerance of ±0.02 mm in an era where electronic control was still yet to be invented. For more than eighty years, we have kept this mill working in fine shape down to the very smallest part. This treasured mill is made in Japan, and has been with us as we pass down the skills and pride of craftworkers from our founder.</p>
      )}
      reverse={true}
    />
    <ColumnBlock02
      wrap01={(
        <figure>
          <img src="/assets/images/history/photo03.png" alt="Original model of the two-high cold rerolling mill, still in use today"/>
          <figcaption>Original model of the two-high cold rerolling mill, still in use today</figcaption>
        </figure>
      )}
      wrap02={(
        <p>In 1973, we moved our head office to Togo-cho in Aichi-ken to a location closer to our major clients, for the purpose of expanding our productivity. In 1984, we celebrated the 50th anniversary of our founding by adding coil rolling to our existing steel plate rolling operations, becoming one of a select few steel rolling manufacturers in Japan.</p>
      )}
      reverse={false}
    />
  </section>
)

export default class History extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page history lang-en">
        <SectionHead
          poster={`/assets/images/history/h1img.png`}
          title={`HISTORY`}
          sub={`Mishina Matsubishi: How Far We've Come`}
          theme={`white`}
        />
        <section>
          <LeadBlock02
            title={HISTORY_TITLE}
            theme={`gold`}
            render={HISTRY_COLUMNS}
          />
        </section>
        <section>
          <LeadBlock01
            title={`COMPANY HISTORY`}
            sub={`Timeline`}
            theme={`gold`}
            render={COMPANY_HISTORY}
          />
        </section>
        <SectionNext
          current={`history`}
          nextid={`06`}
          nextPath={`/en/about`}
          title={`ABOUT US`}
        />
      </div>
    );
  }
}
