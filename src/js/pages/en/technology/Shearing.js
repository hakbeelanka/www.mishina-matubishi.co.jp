import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_shearing01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>Our proprietary equipment helps us make high-precision shearing a reality.</h3>
        <p>Shearing is a process that uses sharp blades to cut off parts of steel plates. At Mishina Matsubishi, we use our own original equipment to ensure that materials are not scratched during manufacture. We also use robots that automate the moving and stacking of materials through suction and magnets, avoiding scratches between processes. We have also taken measures with our equipment to prevent the steel plates from warping and curling after rough cutting before rerolling, enhancing the precision of our work.</p>
      </div>
    )}
    reverse={true}
  />
)

const SHEARING_TECHNOLOGY = (
  <div className={`columnblock03 col_6`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_shearing02.png" alt=""/>
      </figure>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_shearing03.png" alt=""/>
      </figure>
    </div>
  </div>
)

export default class Shearing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`shearing`}
        />
        <SectionItemHead
          poster={`/assets/videos/sharing.mp4`}
          title={`Shearing`}
          sub={`We work with steel with the flexibility of paper.`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={SHEARING_TECHNOLOGY}
            last={true}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
