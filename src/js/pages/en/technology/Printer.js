import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import LeadBlock03 from "../../../components/leadblock_03"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_printer01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>Expanding the possibilities in creating shapes.</h3>
        <p>3D printing is useful in creating prototypes during the design phase and in testing, as even complex shapes can be rapidly created without the need for a mold or jig. Feel free to ask us at Mishina Matsubishi if you need to develop prototypes. We are also involved in the R&D of parts manufacturing using 3D printers.</p>
      </div>
    )}
    reverse={true}
  />
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table`}>
    <tbody>
    <tr>
      <th className="rowhead">Finished product size</th>
      <td>254(W) x 254(D) x 254(H)mm</td>
    </tr>
    <tr>
      <th className="rowhead">Materials used</th>
      <td>ASA/ABS-M30/PLA</td>
    </tr>
    <tr>
      <th className="rowhead">Port materials</th>
      <td>WaterWorks solution support system</td>
    </tr>
    <tr>
      <th className="rowhead">Layer pitch</th>
      <td>0.127/0.178/0.254/0.330mm</td>
    </tr>
    <tr>
      <th className="rowhead">Precision</th>
      <td>±0.200mm または ±0.002mm</td>
    </tr>
    </tbody>
  </table>
)


export default class Printer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`printer`}
        />
        <SectionItemHead
          poster={`/assets/images/technologies/technology/head_printer.png`}
          title={`3D Printing`}
          sub={`Creating 3D shapes with masterful skill.`}
          select={`analysis`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock03
            title={"Product characteristics"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
