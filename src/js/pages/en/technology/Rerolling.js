import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import LeadBlock03 from "../../../components/leadblock_03"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_rerolling01.png" alt="Non-reversible two-high rerolling mill (Head Office, Togo Factory)" />
        <figcaption>Non-reversible two-high rerolling mill (Head Office, Togo Factory)</figcaption>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>The technology to stretch steel, uniformly and evenly.</h3>
        <p>Rerolling is the process of compressing steel plates and coils into rolls, and stretching them into the desired thickness. Even though the finished dimensions may be the same, the quality of steel depends greatly on how many times the material has been rerolled, and at what thickness. At Mishina Matsubishi, we design our processes so that all parts of the steel we process have a uniform thickness and composition, based on the wealth of past data we have accumulated. With our high quality and highly efficient production owing to state-of-the-art facilities that utilize computerized auto-measurement, as well as the ultra-high precision craftsmanship of our experienced workers, we respond to the diverse needs of our customers with our "Made in Japan—Made by Mishina Matsubishi" manufacturing quality that handles single products and small lots.</p>
      </div>
    )}
    reverse={true}
  />
)

const REROLLING_TECHNOLOGY = (
  <div className={`columnblock03 col_4 adjust-head`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_rerolling02.png" alt="AGC"/>
      </figure>
      <h3>AGC<span>(Auto-gap control system)</span></h3>
      <p>This system continuously measures coil thickness during rerolling. The system automatically fine-tunes the gaps in the rerolling roll, bringing the tolerance as close to zero as possible.</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/en/technology/photo_rerolling03.png" alt="Roll bender system"/>
      </figure>
      <h3>Roll bender system</h3>
      <p>An appropriate degree of force is applied to reinforce the steel, as it takes on a crown shape when rerolled using a straight roll.</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_rerolling04.png" alt="Measures taken to eliminate scratches"/>
      </figure>
      <h3>Measures taken to eliminate scratches</h3>
      <p>The output side of our rolling mills uses lighted mirrors that inspect the underside of the material, ensuring that scratched materials never make it through the production line. We also continue to use auto-detection methods via image processing.</p>
    </div>
  </div>
)

const SPEC_TABLE = $(window).innerWidth() > 960 ?
  (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
    <table className={`spec_table`}>
      <tbody>
      <tr className="head">
        <th className="rowhead" colSpan="2"/>
        <th>Product plate thickness</th>
        <th>Plate thickness precision (subject to consultation)</th>
        <th>Width</th>
      </tr>
      <tr>
        <th className="rowhead" rowSpan="2">Toyota plant</th>
        <th className="second">Rerolling unit no. 1</th>
        <td>1.2-7.0mm</td>
        <td>3.0mm or less, ±0.03mm<br/>Over 3.0mm, ±0.05mm</td>
        <td>65mm-330mm</td>
      </tr>
      <tr>
        <th className="second">Rerolling unit no. 2</th>
        <td>0.65-8.0mm</td>
        <td>4.0mm or less, ±0.02mm<br/>Over 4.0mm, ±0.05mm</td>
        <td>50mm-200mm</td>
      </tr>
      <tr>
        <th className="rowhead" colSpan="2">Head office plant</th>
        <td>1.2-12.0mm</td>
        <td>±0.05mm</td>
        <td>50mm-500mm</td>
      </tr>
      <tr>
        <th className="rowhead" rowSpan="2">Thailand plant</th>
        <th className="second">4-layer rolls</th>
        <td>0.8-8.0mm</td>
        <td>4.0mm or less, ±0.02mm<br/>Over 4.0mm, ±0.02mm</td>
        <td>70mm-500mm</td>
      </tr>
      <tr>
        <th className="second">6-layer rolls</th>
        <td>0.1-1.0mm</td>
        <td>±0.007mm</td>
        <td>50mm-500mm</td>
      </tr>
      </tbody>
    </table>
  ) :
  (
    <div>
      <table className={`spec_table`}>
        <tbody>
        <tr className="head">
          <th className="rowhead" colSpan="2"/>
          <th>Product plate thickness</th>
          <th>Plate thickness precision (subject to consultation)</th>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">Toyota plant</th>
          <th className="second">Rerolling unit no. 1</th>
          <td>1.2-7.0mm</td>
          <td>3.0mm or less, ±0.03mm<br/>Over 3.0mm, ±0.05mm</td>
        </tr>
        <tr>
          <th className="second">Rerolling unit no. 2</th>
          <td>0.65-8.0mm</td>
          <td>4.0mm or less, ±0.02mm<br/>Over 4.0mm, ±0.05mm</td>
        </tr>
        <tr>
          <th className="rowhead" colSpan="2">Head office plant</th>
          <td>1.2-12.0mm</td>
          <td>±0.05mm</td>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">Thailand plant</th>
          <th className="second">4-layer rolls</th>
          <td>0.8-8.0mm</td>
          <td>4.0mm or less, ±0.02mm<br/>Over 4.0mm, ±0.02mm</td>
        </tr>
        <tr>
          <th className="second">6-layer rolls</th>
          <td>0.1-1.0mm</td>
          <td>±0.007mm</td>
        </tr>
        </tbody>
      </table>
      <table className={`spec_table`}>
        <tbody>
        <tr className="head">
          <th className="rowhead" colSpan="2"/>
          <th>Width</th>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">Toyota plant</th>
          <th className="second">Rerolling unit no. 1</th>
          <td>65mm-330mm</td>
        </tr>
        <tr>
          <th className="second">Rerolling unit no. 2</th>
          <td>50mm-200mm</td>
        </tr>
        <tr>
          <th className="rowhead" colSpan="2">Head office plant</th>
          <td>50mm-500mm</td>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">Thailand plant</th>
          <th className="second">4-layer rolls</th>
          <td>70mm-500mm</td>
        </tr>
        <tr>
          <th className="second">6-layer rolls</th>
          <td>50mm-500mm</td>
        </tr>
        </tbody>
      </table>
    </div>
  );


export default class Rerolling extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`rerolling`}
        />
        <SectionItemHead
          poster={`/assets/videos/atsuen.mp4`}
          title={`Rerolling`}
          sub={`Thickness is in our hands.`}
          select={`analysis`}
        />
        <section>
          <LeadBlock02
            theme={`gold`}
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={REROLLING_TECHNOLOGY}
          />
        </section>
        <section>
          <LeadBlock03
            title={"Anti-scratch measures taken (for coils)"}
          >
            <picture>
              <img src="/assets/images/technologies/en/technology/photo_rerolling05.png" alt="Anti-scratch measures taken (for coils)" />
            </picture>
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock03
            title={"Rerolling specifications"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList/>
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
