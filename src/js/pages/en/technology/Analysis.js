import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>We offer high-quality products that have undergone rigorous testing and inspection.</h3>
        <p>At Mishina Matsubishi, we thoroughly implement product quality management to satisfy our customers. We use a standalone inspection room to rigorously inspect our products from corner to corner.</p>
      </div>
    )}
    reverse={true}
  />
)

const TESTING_TECHNOLOGY = (
  <div className={`columnblock03 col_3`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis02.png" alt="Durometer"/>
      </figure>
      <h3>Durometer</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis03.png" alt="General-purpose pulling tester"/>
      </figure>
      <h3>General-purpose pulling tester</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis04.png" alt="Surface roughness tester"/>
      </figure>
      <h3>Surface roughness tester</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis05.png" alt="Industrial microscope"/>
      </figure>
      <h3>Industrial microscope</h3>
    </div>
  </div>
)

export default class Rerolling extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`analysis`}
        />
        <SectionItemHead
          poster={`/assets/images/technologies/technology/head_analysis.png`}
          title={`Testing and Inspection`}
          sub={`We offer you peace of mind.`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={TESTING_TECHNOLOGY}
            last={true}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
