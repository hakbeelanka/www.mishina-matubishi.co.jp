import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import LeadBlock03 from "../../../components/leadblock_03"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>Sheet metal working makes steel easier to use.</h3>
        <p>Mishina Matsubishi offers a diverse range of sheet metal work, such as making holes of different sizes, punching, spot welding and more. Please get in touch with us regarding your needs.</p>
      </div>
    )}
    reverse={true}
  />
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table col_4`}>
    <tbody>
    <tr className="head">
      <th>Equipment</th>
      <th>Name commonly used</th>
      <th>Feeder material dimensions and applicable content</th>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="3">Shearing</th>
      <th>Large-scale shearing</th>
      <td>1.2-7.0mm</td>
    </tr>
    <tr>
      <th>Medium-scale shearing</th>
      <td>1.2-7.0mm</td>
    </tr>
    <tr>
      <th>Small-scale shearing</th>
      <td>1.2-7.0mm</td>
    </tr>
    <tr>
      <th className="rowhead">Punching machine</th>
      <td />
      <td className="borderleft">
        Feed stock size: 1,000 x 2,540mm<br />
        Thickness: max. 4.5mm<br />
        Function: Cuts holes in various sizes such as round, square and so on; also performs cutting.
      </td>
    </tr>
    <tr>
      <th className="rowhead">Bending machine</th>
      <td />
      <td className="borderleft">
        Length of bent section: 2000mm<br />
        Thickness: max. 3.2 mm<br />
        Function: Can bend material from 0º to 180º or perform radial bending; can also do double-bends.
      </td>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="2">Press</th>
      <td>30-ton mechanical press</td>
      <td className="borderleft" rowSpan="2">Applies to holes, bending and cutting work.</td>
    </tr>
    <tr>
      <td>60-ton mechanical press</td>
    </tr>
    <tr>
      <th className="rowhead">Other processing devices</th>
      <td colSpan="2">
        Tap drill<br />
        Drill press<br />
        Band paper machine<br />
        Band saw, other equipment
      </td>
    </tr>
    </tbody>
  </table>
)

const SHEETMETAL_TECHNOLOGY = (
  <div className={`columnblock03 col_3`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal02.png" alt="Hole punching of different sizes" />
      </figure>
      <h3>Hole punching of different sizes</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal03.png" alt="Punching and bending combined" />
      </figure>
      <h3>Punching and bending combined</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal04.png" alt="Bending and spot welding combined" />
      </figure>
      <h3>Bending and spot welding combined</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal05.png" alt="Two-step bending and welding of specific parts combined" />
      </figure>
      <h3>Two-step bending and welding of specific parts combined</h3>
    </div>
  </div>
)


export default class SteelMetal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`sheetmetal`}
        />
        <SectionItemHead
          poster={`/assets/images/technologies/technology/head_sheetmetal.png`}
          title={`Sheet metal processing`}
          sub={`Changing the form.`}
          select={`analysis`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={SHEETMETAL_TECHNOLOGY}
          />
        </section>
        <section>
          <LeadBlock03
            title={"Processing equipment specifics"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
