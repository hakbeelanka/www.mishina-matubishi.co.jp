import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_pickling01.png" alt="Our steel plate handling robot sets each cartridge automatically, one at a time."/>
        <figcaption>Our steel plate handling robot sets each cartridge automatically, one at a time.</figcaption>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>We remove scaling using liquid sulfuric acid.</h3>
        <p>Pickling is a process by which any scaling (oxidation film) that has adhered to the surface of the material is removed. The merits of using liquid sulfuric acid are that we can remove scaling from within even the slightest indentations in the metal more easily than with the short blast* method. Our facilities feature a large vat in which we can perform this pickling on plates cut up to 4 m in length. We use a robot system and other methods to remove and handle the steel plates. Our intention is to reduce labor for work that does not require human experience and skill. The iron particles that have floated to the bottom of the waste liquid after use are removed and recycled as a renewable resource. <span>*Short blast: This is a surface treatment method for applying a high-speed impact of dense metallic particles to remove scaling.</span></p>
      </div>
    )}
    reverse={true}
  />
)

const CUTTING_TECHNOLOGY = (
  <div className={`columnblock03 col_6`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_pickling02.png" alt="The steel is processed in the sulfuric acid vat, water cleaning vat and finally the neutralizing vat, in that order.All of these vats can handle steel plates up to 4 m in length."/>
      </figure>
      <p>The steel is processed in the sulfuric acid vat, water cleaning vat and finally the neutralizing vat, in that order. All of these vats can handle steel plates up to 4 m in length.</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/en/technology/photo_pickling03.png" alt="Scaling (the oxidation layer) on the surface of the steel plate is peeled away and removed by the sulfuric acid.Liquid sulfuric acid reaches even the most minute indentations."/>
      </figure>
      <p>Scaling (the oxidation layer) on the surface of the steel plate is peeled away and removed by the sulfuric acid. Liquid sulfuric acid reaches even the most minute indentations.</p>
    </div>
  </div>
)

export default class Pickling extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`pickling`}
        />
        <SectionItemHead
          poster={`/assets/videos/sansen_1.mp4`}
          title={`Pickling`}
          sub={`Refining metals with acid.`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={CUTTING_TECHNOLOGY}
            last={true}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
