import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import LeadBlock03 from "../../../components/leadblock_03"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_annealing01.png" alt="At our Toyota No. 1 plant, we have installed an annealing furnace pit for use in processing coils." />
        <figcaption>At our Toyota No. 1 plant, we have installed an annealing furnace pit for use in processing coils.</figcaption>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>Our technology disperses the carbide to create the hardness you need.</h3>
        <p>Annealing is one of the core technologies of Mishina Matsubishi. We apply heat treatment to the metal in order to disperse the pearlite in the internal structure as spheroids (cementite), thereby "enhancing" the steel to create the desired composition and hardness. This makes the material easier for our customers to press, cut, plate and so on, achieving a uniform quality when quenching. We use non-oxidizing bright annealing according to the temperature conditions, which is a technology we have developed over many long years of experience.</p>
      </div>
    )}
    reverse={true}
  />
)

const ANNEALING_FIGURES = (
  <div className={`columnblock03 col_8-4`}>
    <div>
      <div className={`big`}>
        <div>
          <figure>
            <img src="/assets/images/technologies/technology/photo_annealing02.png" alt="Before annealing" />
          </figure>
          <h3>Before annealing</h3>
        </div>
        <div>
          <figure>
            <img src="/assets/images/technologies/technology/photo_annealing03.png" alt="After annealing" />
          </figure>
          <h3>After annealing</h3>
        </div>
      </div>
      <p>The black spots shown in the diagram at upper left are oxidants (cementite) in the metal’s structure prior to annealing. By applying spheroid annealing to the material, these particles are finely ground down and become uniform spheroids in shape (shown at upper right).</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_annealing04.png" alt="Large-scale annealing furnace" />
      </figure>
      <h3>Large-scale annealing furnace</h3>
      <p>We have introduced a large-scale annealing furnace at our head office and factory in Togo. This allows us to handle coil annealing as well, focusing on steel plates.</p>
    </div>
  </div>
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table col_5`}>
    <tbody>
    <tr className="head">
      <th className="rowhead"/>
      <th>Material</th>
      <th>Coil external diameter (max.)</th>
      <th>Steel plate (max.)</th>
      <th>Weight</th>
    </tr>
    <tr>
      <th className="rowhead">Toyota plant</th>
      <td>Steel strips only</td>
      <td>Φ1380mm</td>
      <td />
      <td>6,000kg/3 furnaces</td>
    </tr>
    <tr>
      <th className="rowhead">Head office plant</th>
      <td>Steel plates and strips only</td>
      <td>Φ1400mm</td>
      <td>1400(W) x 3900(L) x 700(T)</td>
      <td>15,000kg</td>
    </tr>
    <tr>
      <th className="rowhead">Thailand plant</th>
      <td>Steel strips only</td>
      <td>Φ1600mm</td>
      <td />
      <td>10,000kg/3 furnaces</td>
    </tr>
    </tbody>
  </table>
)

export default class Annealing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`annealing`}
        />
        <SectionItemHead
          poster={`/assets/videos/syoudon.mp4`}
          title={`Annealing`}
          sub={`We keep precise control of the heat applied.`}
        />
        <section>
          <LeadBlock02
            theme={`gold`}
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={ANNEALING_FIGURES}
          >
          </LeadBlock02>

        </section>
        <section>
          <LeadBlock03
            title={"Annealing specifications"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
