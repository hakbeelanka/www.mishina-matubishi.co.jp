import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import LeadBlock03 from "../../../components/leadblock_03"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_cutting01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>Cutouts via press cut are easily within reach.</h3>
        <p>The levelling and cutting process involves cutting out the steel plate from a coil. We correct the tendency for coils to curl due to levelling, making them perfectly flat for cutting. We use two lines, one for press cuts and another for flying shear cutting, making it possible to cut thicknesses of steel plates exceeding 9 mm. Our press cutting machine allows us to handle blanking of freeform shapes. Mishina Matsubishi has a history of manufacturing bicycle parts using this press, with a proven track record in press cutting.</p>
      </div>
    )}
    reverse={true}
  />
)

const CUTTING_TECHNOLOGY = (
  <div className={`columnblock03 col_6`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_cutting02.png" alt="Our press cutting machine can shear thick material, so we can do blanking of freeform shapes as well simply by changing the die."/>
      </figure>
      <p>Our press cutting machine can shear thick material, so we can do blanking of freeform shapes as well simply by changing the die.</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/en/technology/photo_cutting03.png" alt="The leveler in our manufacturing line uses rollers of different sizes.This lets us make delicate changes in rolling force to carefully correct any bending in the material."/>
      </figure>
      <p>The leveler in our manufacturing line uses rollers of different sizes. This lets us make delicate changes in rolling force to carefully correct any bending in the material.</p>
    </div>
  </div>
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table`}>
    <tbody>
    <tr className="head">
      <th className="rowhead" colSpan="2"/>
      <th>Material thickness</th>
      <th>Material width</th>
      <th>Internal diameter</th>
      <th>External diameter (max.)</th>
      <th>Weight (max.)</th>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="2">Toyota plant</th>
      <th className="second">Leveler no. 1</th>
      <td>2.0-6.0mm</td>
      <td>80-350mm</td>
      <td>Φ508mm</td>
      <td>Φ1500mm</td>
      <td>2,800kg</td>
    </tr>
    <tr>
      <th className="second">Leveler no. 2</th>
      <td>2.0-9.0mm</td>
      <td>50-400mm</td>
      <td>Φ508,Φ610mm</td>
      <td>Φ1500mm</td>
      <td>3,000kg</td>
    </tr>
    <tr>
      <th className="rowhead" colSpan="2">Thailand plant</th>
      <td>1.0-10.0mm</td>
      <td>70-500mm</td>
      <td>Φ508,Φ610mm</td>
      <td>Φ1600mm</td>
      <td>8,000kg</td>
    </tr>
    </tbody>
  </table>
)

const SPEC_TABLE2 = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table adjust_cutting_table`}>
    <tbody>
    <tr className="head">
      <th className="rowhead" colSpan="2"/>
      <th>Product length</th>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="2">Toyota plant</th>
      <th className="second">Leveler no. 1</th>
      <td>700-3000mm</td>
    </tr>
    <tr>
      <th className="second">Leveler no. 2</th>
      <td>65-3400mm</td>
    </tr>
    <tr>
      <th className="rowhead" colSpan="2">Thailand plant</th>
      <td>50-3000mm</td>
    </tr>
    </tbody>
  </table>
)

const TABLE_COMBINE = (
  <div>
    {SPEC_TABLE}
    {SPEC_TABLE2}
  </div>
)

export default class Cutting extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`cutting`}
        />
        <SectionItemHead
          poster={`/assets/videos/leberacut.mp4`}
          title={`Levelling & Cutting`}
          sub={`We offer a diverse variety of cutting.`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={CUTTING_TECHNOLOGY}
          />
        </section>
        <section>
          <LeadBlock03
            title={"Leveler specifications"}
            render={TABLE_COMBINE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
