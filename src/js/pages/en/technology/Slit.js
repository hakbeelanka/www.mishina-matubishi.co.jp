import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../../components/section_head_technology"
import SectionItemHead from "../../../components/section_itemhead"
import LeadBlock01 from "../../../components/leadblock_01"
import LeadBlock02 from "../../../components/leadblock_02"
import LeadBlock03 from "../../../components/leadblock_03"
import ColumnBlock02 from "../../../components/columnblock02"
import TechnologyList from "../../../components/technologyList_en"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_slit01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>We offer proven ISO quality.</h3>
        <p>Slitting involves dividing the width of metal coils. While processing the materials as finely as 10 microns, we also check the dimensions, hardness and surface roughness by sampling; and our veteran workers do a visual recheck on each product to make sure there are no scratches. Mishina Matsubishi offers a proven level of product quality based on the ISO9001 quality management system.</p>
      </div>
    )}
    reverse={true}
  />
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table`}>
    <tbody>
    <tr className="head">
      <th className="rowhead" />
      <th>Material thickness</th>
      <th>Material width</th>
      <th>Internal diameter</th>
      <th>External diameter (max.)</th>
      <th>Weight (max.)</th>
    </tr>
    <tr>
      <th className="rowhead">Toyota plant</th>
      <td>2.0-6.0mm</td>
      <td>50-300mm</td>
      <td>Φ508mm</td>
      <td>Φ1500mm</td>
      <td>2,800kg</td>
    </tr>
    <tr>
      <th className="rowhead">Thailand plant</th>
      <td>1.0-10.0mm</td>
      <td>50-500mm</td>
      <td>Φ508,610mm</td>
      <td>Φ1600mm</td>
      <td>8,000kg</td>
    </tr>
    </tbody>
  </table>
)


export default class Slit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHeadTechnology
          icon={`slit`}
        />
        <SectionItemHead
          poster={`/assets/videos/slit.mp4`}
          title={`Slitting`}
          sub={`We work with steel with the flexibility of paper.`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock03
            title={"Slitter specifications"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
