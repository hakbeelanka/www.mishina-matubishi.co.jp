import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../../components/section_head"
import SectionNext from "../../components/section_next"
import LeadBlock01 from "../../components/leadblock_01"
import TechnologyList from "../../components/technologyList_en"

const TECHNOLOGY_QUALITY = (
  <ul className={`technology_quality`}>
    <li>
      <h3>ISO9001/14001 certification</h3>
      <p>Mishina Matsubishi acquired its ISO9001 and ISO14001 certifications in 2002. As a company that coexists with the local community, we have a responsibility to continue the pursuit of quality, while at the same time reduce our burden on the Earth's environment. Our conviction is what led us to acquiring our two ISO certifications at the same time.</p>
    </li>
    <li>
      <h3>Initiatives regarding quality and the environment</h3>
      <p>We perform sampling for all of our products in our manufacturing process, and we have created a management system that includes rigorous inspections. We also collect data for the entire length of the coil, allowing us to guarantee quality for the full product length.</p>
    </li>
    <li>
      <h3>Traceability through our infotech-powered system</h3>
      <p>We use an information technology-powered system in-house to maintain a history of the processes used on the steel plates and coils that we purchase from steel manufacturers, all the way through shipment. In the unlikely event that we find a defect in a product after it has been delivered, we can go all the way back to the raw materials to perform a thorough investigation of the problem.</p>
    </li>
  </ul>
)

export default class Technologies extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {}

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies lang-en">
        <SectionHead
          poster={`/assets/images/technologies/h1img.png`}
          title={`TECHNOLOGY`}
          sub={`An Introduction to Our Technology`}
          theme={`white`}
        />
        <section>
          <div className={`heroblock04`}>
            <h1>We continue to refine our technologies for controlling steel,<br className="only-pc"/> to bring our customers the best products possible.
            </h1>
            <p>
              At the essence of Mishina Matsubishi's technology is how we create the composition of metal to precisely meet our customer's needs.<br/>
              We design and process each product to fulfill the intended use with the optimum structure.<br/>
              Our highly useable products come off the line looking beautiful, at the quantity and dimensions our customers request.
            </p>
          </div>
        </section>
        <section>
          <LeadBlock01
            title={`MANUFACTURING PROCESS`}
            sub={`A manufacturing process that makes optimization a reality`}
            theme={`black`}
            render={TECHNOLOGY_PROCESS}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`An Introduction to Our Technology`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
        <section>
          <LeadBlock01
            title={`QUALITY CONTROL AND ASSURANCE SYSTEM`}
            sub={`Quality management and warranty system`}
            theme={`white`}
            render={TECHNOLOGY_QUALITY}
          />
        </section>
        <SectionNext
          current={`technologies`}
          nextid={`04`}
          nextPath={`/en/products`}
          title={`PRODUCTS`}
        />
      </div>
    );
  }
}

const TECHNOLOGY_PROCESS = (
  <div className={`technology_process`}>
    <ul className={`factory`}>
      <li>
        <h3>Head office plant</h3>
        <p>Our head office plant is primarily involved in the production of ultra-high-precision products with a high level of added value.</p>
        <picture className={`process_image`}>
          <source srcSet="/assets/images/technologies/en/process_honsha.png"
                  media="(min-width: 960px)" />
          <img src="/assets/images/technologies/en/process_honsha-sp.png" alt="car"/>
        </picture>
        <div className={`factory_photos`}>
          <picture>
            <img src="/assets/images/technologies/photo01_honsha.png" alt="本社工場" />
          </picture>
          <picture>
            <img src="/assets/images/technologies/photo02_honsha.png" alt="本社工場" />
          </picture>
        </div>
        <div className={`links`}>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_TOGO.pdf">Togo Plant：Equipment specification list<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
        </div>
      </li>
      <li>
        <h3>Toyota plant, Thailand plant</h3>
        <p>Our Toyota and Thailand plants are primarily involved in processing industrial and mass-production products.</p>
        <picture className={`process_image`}>
          <source srcSet="/assets/images/technologies/en/process_other.png"
                  media="(min-width: 960px)" />
          <img src="/assets/images/technologies/en/process_other-sp.png" alt="car"/>
        </picture>
        <div className={`factory_photos`}>
          <picture>
            <img src="/assets/images/technologies/photo01_other.png" alt="豊田工場・タイ工場" />
          </picture>
          <picture>
            <img src="/assets/images/technologies/photo02_other.png" alt="豊田工場・タイ工場" />
          </picture>
        </div>
        <div className={`links`}>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_TOYOTA01.pdf">Toyota Plant No. 1：Equipment specification list<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_TOYOTA02.pdf">Toyota Plant No. 2：Equipment specification list<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
          <div>
            <div className="btn__more">
              <a href="/assets/SPEC_Thailand.pdf">Thailand Plant：Equipment specification list<img src="/assets/images/icon_pdf.png" alt="pdf" /></a>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
)
