import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"
import {Link} from "react-router-dom"
import Particles from 'react-particles-js';

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount(){}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
    if (this.refs.mainVd && this.refs.clipVd) {
      this.refs.mainVd.play();
      this.refs.clipVd.play();
    }

  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    let particleNumber = (this.state.width > 860) ? 80: 20;
    
    let calculateSize;
    if (this.state.width > 860) {
      if (this.state.height / this.state.width < 1080/1920){
        calculateSize = `width: 100%`;
      } else {
        calculateSize = `height: 100%`;
      }
    } else {
      if (this.state.height > this.state.width){
        calculateSize = `width: 100%`;
      } else {
        calculateSize = `width: 100%`;
      }
    }

    let movieHeroSwitcher = () => {
      if (this.state.width > 860) {
        return (
          <div className={`bg_video`}>
            <video
              src="/assets/videos/mishina_30mb.mp4"
              ref="mainVd"
              loop={true}
              muted={true}
            />
            <div className="video__clip">
              <video
                ref="clipVd"
                src="/assets/videos/mishina_30mb.mp4"
                loop={true}
                muted={true}
              />
              <svg viewBox="0 0 1000 555" className="svg-overlay">
                <defs>
                  <clipPath id="mask">
                    <rect className="cls-1" x="117.29" y="55.96" width="78.05" height="13.99"/>
                    <rect className="cls-1" x="117.29" y="79.53" width="78.05" height="13.99"/>
                    <rect className="cls-1" x="117.29" y="103.09" width="78.05" height="13.99"/>
                    <rect className="cls-1" x="263.82" y="77.6" width="48.61" height="19.31" rx="9.65" ry="9.65"/>
                    <path className="cls-1"
                          d="M250.2,120.87c-.54.45-1,.24-1-.47V54c0-.71.45-.92,1-.47l39.53,32.82a1,1,0,0,1,0,1.64Z"/>
                    <path className="cls-1"
                          d="M257.58,73S180,49.46,131.31.78c-2-2-5.1.1-3.11,2.88,1.09,1.51,54.88,65.82,132.56,85.92C272.57,86.05,257.58,73,257.58,73Z"/>
                    <path className="cls-1"
                          d="M257.58,99.49S180,123,131.31,171.69c-2,2-5.1-.1-3.11-2.88,1.09-1.51,54.88-65.83,132.56-85.92C272.57,86.42,257.58,99.49,257.58,99.49Z"/>
                    <rect className="cls-1" y="77.6" width="48.6" height="19.31" rx="9.65" ry="9.65"/>
                    <path className="cls-1"
                          d="M62.22,120.87c.54.45,1,.24,1-.47V54c0-.71-.45-.92-1-.47L22.7,86.39a1,1,0,0,0,0,1.64Z"/>
                    <path className="cls-1"
                          d="M54.84,73S132.42,49.46,181.11.78c2-2,5.11.1,3.1,2.88-1.07,1.51-54.86,65.82-132.54,85.92C39.85,86.05,54.84,73,54.84,73Z"/>
                    <path className="cls-1"
                          d="M54.84,99.49s77.58,23.53,126.27,72.2c2,2,5.11-.1,3.1-2.88C183.14,167.3,129.35,103,51.67,82.89,39.85,86.42,54.84,99.49,54.84,99.49Z"/>
                  </clipPath>
                </defs>
              </svg>
            </div>
          </div>
        )
        // this.refs.mainVd.play();
        // this.refs.clipVd.play();
      } else {
        return (
          <div className={`bg_video`}>
            <video
              src="/assets/videos/mishina_30mb.mov"
              ref="mainVd"
              loop={true}
              muted={true}
              autoPlay={true}
              playsInline={true}
            />
          </div>
        )
        // this.refs.mainVd.play();
      }
    }

    let movieEarthSwitcher = () => {
      if (this.state.width > 860) {
        return (
          <video src="/assets/videos/earth.mp4" autoPlay={true} muted loop />
        )
      } else {
        return (
          <img src="/assets/videos/earth.gif" alt="earth" />
        )
      }
    }

    return Style.it(`
          .page__container .home_lead {
            height: ${this.state.height}px;
          }
          .page__container:before {
            border-right: ${this.state.width}px solid transparent;
            bottom: ${this.state.width * 0.74}px;
          }
          // .video__clip svg {
          //   width: ${this.state.width}px;
          //   height: ${this.state.height}px;
          // }
          #mask {
            transform: scale(3) translate(${this.state.width / 17}px,${this.state.height / 17}px);
          }
          video {
            ${calculateSize}
          }
          `,
      <div className="page home lang-en">
        <section className={`hero`} style={{height: `${this.state.height}px`}}>
          {movieHeroSwitcher()}
          <div className="copy">
            <h1><img src="/assets/images/top/en/title.png" alt="KANTETSU ~Beyond steel~"/></h1>
          </div>
          <button className={`btn__scroll`}>SCROLL</button>
        </section>
        <section>
          <div className="heroblock01 is-phlosophy" style={{backgroundColor: 'rgba(255,255,255,0.83)'}}>
            <div data-index="01" className="heroblock01__heading">
              <h2>PHILOSOPHY</h2>
              <h3>Our Unbending Principles</h3>
            </div>
            <div className="heroblock01__lead">
              <h1>A Resolve That' s as Strong as Steel</h1>
              <p>Mishina Matsubishi was founded over eight decades ago. <br />From long ago, steel has played a vital role as "the rice of industry. "
At Mishina Matsubishi, we have excelled in our work with steel. As a total manufacturer of steel plates and steel materials, there are fundamentals that we firmly adhere to even today.
              </p>
              <div className="btn__more">
                <Link to="/en/philosophy">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01" style={{backgroundColor: 'rgb(0,0,0)'}}>
            {movieEarthSwitcher()}
            <Particles
              params={{
                particles: {
                  number: {
                    value: particleNumber
                  }
                }
              }}
            />
            <div data-index="02" className="heroblock01__heading">
              <h2>VISION</h2>
              <h3>Our Dreams for the Future</h3>
            </div>
            <div className="heroblock01__lead is-right">
              <h1>Harnessing the uniqueness of metal</h1>
              <p>Mishina Matsubishi supplies high-precision cold-rolled steel strips with a beautiful shine in small lots. <br />
                As a unique presence in the steel industry that stands out from the rest, our future is brimming with imagination and creativity.
              </p>
              <div className="btn__more">
                <Link to="/en/vision">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01 is-technology" style={{
            backgroundImage: `url(${'/assets/images/top/bg_03.png'})`
          }}>
            <div data-index="03" className="heroblock01__heading">
              <h2>TECHNOLOGY</h2>
              <h3>An Introduction to Our Technology</h3>
            </div>
            <div className="heroblock01__lead is-right">
              <h1 className="is-adjust">Technological skill that makes "optimization" come true</h1>
              <p>You might want stronger steel materials,<br />
                or several different thicknesses. <br />
                Mishina Matsubishi's core technology is creating the optimum composition of metal to suit your needs.
              </p>
              <div className="btn__more">
                <Link to="/en/technologies">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01" style={{backgroundImage: `url(${'/assets/images/top/bg_04.png'})`}}>
            <div data-index="04" className="heroblock01__heading">
              <h2>PRODUCTS</h2>
              <h3>About Our Products</h3>
            </div>
            <div className="heroblock01__lead">
              <h1>What we do for our customers</h1>
              <p>The steel that Mishina Matsubishi offers is not just simple material. <br />
                Our products represent the value that we offer, through working together with our customers to solve their problems and open up the doors to new possibilities.
              </p>
              <div className="btn__more">
                <Link to="/en/products">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section>
          <div className="heroblock01 is-history" style={{backgroundImage: `url(${'/assets/images/top/bg_05.png'})`}}>
            <div data-index="05" className="heroblock01__heading is-white">
              <h2>HISTORY</h2>
              <h3>Mishina Matsubishi: How Far We've Come</h3>
            </div>
            <div className="heroblock01__lead">
              <img src="/assets/images/top/history.png" alt="history"/>
              <h1>Our History: Refining Our Expert Craftsmanship</h1>
              <p>Mishina Matsubishi's history began seven years prior to the Pacific War in 1934. <br />We'd like to introduce you to our long history of refining our expert craftsmanship, with our contributions to the fields of aviation, bicycles, automobiles and motorcycles.
              </p>
              <div className="btn__more">
                <Link to="/en/history">READ MORE</Link>
              </div>
            </div>
          </div>
        </section>

        <section className={`flex-row`}>
          <div className="heroblock02" style={{backgroundImage: `url(${'/assets/images/top/bg_06.png'})`}}>
            <Link to="/en/about">
              <div data-index="06" className="heroblock02__heading">
                <h2>ABOUT US</h2>
                <h3>Company Overview</h3>
              </div>
            </Link>
          </div>
          <div className="heroblock02" style={{backgroundImage: `url(${'/assets/images/top/bg_07.png'})`}}>
            <Link to="/en/recruit">
              <div data-index="07" className="heroblock02__heading">
                <h2>CAREER</h2>
                <h3>Career Information</h3>
              </div>
            </Link>
          </div>
        </section>

        <section>
          <div className="heroblock01 is-future" style={{backgroundColor: '#e1e2e5'}}>
            <img src="/assets/images/top/future.png" alt="Future"/>
            <div data-index="08" className="heroblock01__heading">
              <h2>FUTURE</h2>
              <h3>Now Beginning!- Our Project to Create the Future</h3>
            </div>
            <div className="heroblock01__lead">
              <p>
                Our challenge in making our future come true has already begun. <br />
                Mishina Matsubishi is taking action towards crafting a brand-new future with ideas that are amazingly unique.
              </p>
              <div className="btn__more is-blank">
                <a href="https://www.mishina-and-co.com/miraisouzou/" target={`_blank`}>READ MORE</a>
              </div>
            </div>
          </div>
        </section>

      </div>
    );
  }
}
