import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../../components/section_head"
import SectionNext from "../../components/section_next"

export default class Vision extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount(){}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page vision">
        <SectionHead
          poster={`/assets/images/vision/h1img.png`}
          title={`VISION`}
          sub={`Our Dreams for the Future.`}
          theme={`white`}
        />
        <section>
          <div className={`deco1`} />
          <div className={`deco2`} />
          <div className={`heroblock04`}>
            <h1>Why we can make our dreams come true.</h1>
            <p>
              Mishina Matsubishi is simultaneously involved in the production of single high-spec products that are precise down to the micron level, and mass production processing at a high level of quality.<br />
              With one foot firmly planted in steel, we rise to the challenge of new businesses, in the pursuit of new possibilities.<br />
              It's our pride in the technical skill we've achieved through the years, as well as our goal to contribute to the development of global industry that allow us to move forward with our "kantetsu (carry out)" philosophy.<br />
              There are certain dreams that only Mishina Matsubishi can fulfill, as one of the precious few specialty manufacturers of steel rerolling in Japan that knows all about steel.
            </p>
          </div>
          <div className={`heroblock05 vision-01`}>
            <div className={`heroblock05__heading is-index`} data-index={`01`}>
              <h2 data-sub={`FLEXIBLE`}>We use the same facilities as blast furnace <br className="only-pc"/>manufacturers do, with more flexible operations.</h2>
            </div>
            <div className={`heroblock05__content`}>
              <p>How can Mishina Matsubishi produce high-precision steel stock in small quantities?It's because we use the same workflow as large-scale steel manufacturers who have their own blast furnaces, but our facilities are at a smaller scale. We are able to cover all the bases in the production workflow, including rerolling, annealing, pickling, leveling & cutting, shirring and slitting work. There are very few other manufacturers in Japan that can do both steel plates and coil rolling. One of Mishina Matsubishi's strong points is that we can do cold rerolling for both steel plates and coils, a process necessary to produce polished steel stock with a uniform surface and a beautiful shine. Our expert craftworkers use our small-scale facilities to masterfully offer a diverse range of processing, so that we can offer small-lot production all the way down to a single steel plate or coil.</p>
            </div>
          </div>
          <div className={`heroblock05 is-white vision-02`}>
            <div className={`heroblock05__heading`} data-index={`02`}>
              <h2 data-sub={`MATERIAL`}>We alter the qualities of steel to <br className="only-pc"/>expand its possibilities giving you <br className="only-pc"/>beautiful steel that's easy to use.</h2>
            </div>
            <div className={`heroblock05__content`}>
              <p>At Mishina Matsubishi, we receive many special orders to manufacture exceedingly difficult products, such as prototypes of automotive accessories still in development. The core technology we apply is our ability to alter the quality of the steel itself. The structure of metal can vary dramatically, depending on how many times the steel is rerolled and at what thickness, and on how many times the metal is treated and at what temperature. Our ability to freely change steel into the structure required is one of our assets, built over many years. As we further refine our technology for altering the composition of steel, we are involved in the manufacture of steel plates that are even thinner as well as the processing of special kinds of steel. We also aim to establish peripheral technologies such as rolling of clad material, surface treatment, die-cutting using a blanking press and more. Mishina Matsubishi is also meeting the challenge in R&D and manufacture of new functional materials,and we are researching technologies with a view towards the future of parts manufacturing.</p>
            </div>
          </div>
          <div className={`heroblock05 vision-03`}>
            <div className={`heroblock05__heading`} data-index={`03`}>
              <h2 data-sub={`INTERNATIONAL`}>Steel processing adds high value in Japan. <br className="only-pc"/>We mass-produce Japan quality-products overseas.</h2>
            </div>
            <div className={`heroblock05__content`}>
              <p>Mishina Matsubishi brings a high level of "Made In Japan" quality to markets around the world. We opened our resident representative office in Bangkok, the capital of Thailand in June 2011, and established a local company called Mishina Matsubishi (Thailand) in the regional city of Chonburi located in the outskirts of Bangkok on January 2012. At the Thailand plant, mass-produced products are processed, and we are able to produce ultra-thin cold rolled steel stock at a thickness of 0.1 mm. Our aim is to expand our sales to China, Indonesia, Malaysia, Vietnam and other Asian countries from our base in Thailand; and we want to create a system to supply to the rest of the world as well, such as North America. While we expand our products onto the global stage, we are also pouring our efforts into the production of ultra-high-precision, high value-added products in Japan, centered on our head office plant. With our two different strategies for Japan and for overseas markets,<br className="only-pc"/>we want to contribute to the growth of industry on a global scale.</p>
            </div>
          </div>
          <div className={`heroblock05 vision-04`}>
            <div className={`heroblock05__heading `} data-index={`04`}>
              <h2 className={`is-white`} data-sub={`CHALLENGE`}>Our challenge to ourselves for <br className="only-pc"/> the next century, beyond honing<br className="only-pc"/> ur craft with steel.</h2>
            </div>
            <div className={`heroblock05__content is-white`}>
              <p>It took eighty long years for Mishina Matsubishi to acquire the technology we possess today.<br />
                Supposing that it takes another century to make our ideal become reality, there's no better time than the present for us to face the challenges for that future.<br />
                We're staking all of the technical skill that we have cultivated through the present on creating our new dream:bringing people and steel closer together.<br />
                This is a challenge that's both unique and creative.</p>
            </div>
          </div>
          <div className={`heroblock06`}>
            <ul>
              <li data-sub="Our #1 new challenge">
                <h3>Drawing out the power of metal through chemistry.</h3>
                <p>One of the aspects of our work involves chemistry, as the power of chemistry is needed during the pickling process, part of the steel production workflow. By applying this kind of chemistry, we have developed reduced steel and chelated steel. Although these materials have the properties of metal, they are more flexible in terms of processing and shaping, and can be used in a variety of applications. Our initial challenge is in creating agricultural fertilizer that uses mineral components. This will serve as our inroad to expanding into the biotechnology business.</p>
              </li>
              <li data-sub="Our #2 new challenge">
                <h3>Taking to the skies by developing new materials and technology</h3>
                <p>This challenge means going deeper with our core technologies, and developing new materials and parts manufacturing technologies. We will use these technologies to move into the aerospace business, such as drones and aircraft. The field of aviation brings us back to our roots as a company. As we make good on our original aims through the "kantetsu (carry out)" philosophy, we will move into the fields of medicine, food and housing, paving the way for a future with people and steel.</p>
              </li>
            </ul>
          </div>
        </section>
        <SectionNext
          current={`vision`}
          nextid={`03`}
          nextPath={`/en/technologies`}
          title={`TECHNOLOGY`}
        />
      </div>
    );
  }
}
