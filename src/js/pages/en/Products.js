import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHead from "../../components/section_head"
import SectionNext from "../../components/section_next"
import LeadBlock01 from "../../components/leadblock_01"

const PRODUCTS_TYPE = (
  <div>
    <ul className={`products_type`}>
      <li><h3 data-num="(1)">Ordinary steel</h3>
        <p>SPHC/SPHD/SPHE/SAPH/SS400, etc.</p>
      </li>
      <li><h3 data-num="(2)">Special steel</h3>
        <p>SAE/SC/SK/SCr/SCM, etc.</p>
      </li>
      <li><h3 data-num="(3)">Cold-rolled steel strips</h3>
        <p>SPCC/SPCD/SPCE/SAPH/SS400/SAE/SC/SK/SCr/SCM, etc.</p>
      </li>
      <li><h3 data-num="(4)">Surface-treated material</h3>
        <p>SGHC/SGAHC/ZAM etc.</p>
      </li>
      <li><h3 data-num="(5)">Stainless steel</h3>
        <p>SUS304/SUS409/ZAM etc.</p>
      </li>
      <li><h3 data-num="(6)">Nonferrous metal</h3>
        <p>Copper/Aluminum/Titanium etc.</p>
      </li>
    </ul>
    <p className="products_type_caption">Please inquire even steel types not listed.</p>
  </div>
)

const PRODUCTS_SOLUTION = (
  <ul className={`products_solution`}>
    <li><h3>We can't find steel plates at the thickness we need.</h3>
      <p>Through rerolling, Mishina Matsubishi can create steel plates and strips at the thickness you desire.</p>
    </li>
    <li><h3>We only need deliveries in small lots.</h3>
      <p>Mishina Matsubishi can handle orders as little as one large steel plate or a 1C slitted coil (50 mm or wider).</p>
    </li>
    <li><h3>We need it right away.</h3>
      <p>We'll take care of it! Our processes are flexible enough to handle quick turnarounds.</p>
    </li>
    <li><h3>We can't find steel plates and sheets of the quality we require.</h3>
      <p>Mishina Matsubishi will coordinate with you to build a workflow that can handle the standards, hardness, surface roughness and panel thickness tolerances you need to meet. </p>
    </li>
    <li><h3>We need to remanufacture a product from items already stocked.</h3>
      <p>Mishina Matsubishi can remanufacture materials that have been stocked for a long time. We can also handle cold rolling, annealing and more.</p>
    </li>
    <li><h3>We need materials with a lustrous finish.</h3>
      <p>Mishina Matsubishi finishes our products using bright cold rolling, offering a smooth, glossy finish.<br />
        (Example: at or below Ra 0.4)<br />
        Our equipment uses lighted mirrors and other methods to ensure that scratched materials never make it through the production line. <br />
        We also use auto-detection methods with image processing.
      </p>
    </li>
    <li><h3>We need materials that are exceptionally easy to process.</h3>
      <div>
        <picture>
          <img src="/assets/images/products/solution02.png" alt="Mishina Matsubishi proudly offers annealing technology with a spheroidizing ratio of over 90%, making pressing and bending possible."/>
        </picture>
        <picture>
          <img src="/assets/images/products/solution01.png" alt="Mishina Matsubishi proudly offers annealing technology with a spheroidizing ratio of over 90%, making pressing and bending possible."/>
        </picture>
      </div>
      <p>Mishina Matsubishi proudly offers annealing technology with a spheroidizing ratio of over 90%, making pressing and bending possible. <br />
        We use non-oxidizing bright annealing according to the temperature conditions, a technology we have developed over many long years of experience.<br />
        The black spots shown in the diagram at upper left are oxidants (cementite) in the metal's structure prior to annealing. <br />
        By rerolling and annealing the material, these particles are finely ground down and become uniform spheroids in shape (shown at upper right).
      </p>
    </li>
  </ul>
)

const PRODUCTS_AVAILABLE = (
  <div className={`products_available`}>
    <ul className={`type`}>
      <li>Special steel + ordinary steel</li>
      <li>Special steel</li>
      <li>Expanded manufacturing ability</li>
      <li>Orders requiring study prior to acceptance</li>
    </ul>
    <ul className={`area`}>
      <li>
        <h3>Cold-rolled strips (sheets)</h3>
        <picture>
          <img src="/assets/images/products/en/available01.png" alt="Cold-rolled strips (sheets)"/>
        </picture>
      </li>
      <li>
        <h3>Cold-rolled strips (coils)</h3>
        <picture>
          <img src="/assets/images/products/en/available02.png" alt="Cold-rolled strips (coils)"/>
        </picture>
      </li>
      <li>
        <h3>Levelling & cutting</h3>
        <picture>
          <img src="/assets/images/products/en/available03.png" alt="Levelling & cutting"/>
        </picture>
      </li>
      <li>
        <h3>Pickling</h3>
        <picture>
          <img src="/assets/images/products/en/available04.png" alt="Pickling"/>
        </picture>
      </li>
    </ul>
  </div>
)
export default class History extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page products lang-en">
        <SectionHead
          poster={`/assets/images/products/h1img.png`}
          title={`PRODUCTS`}
          sub={`About Our Products`}
          theme={`white`}
        />
        <section className={`products_main`}>
          <h2>
            Ask us anything<br /> with our products, we'll provide an answer.
          </h2>
        </section>
        <section>
          <LeadBlock01
            title={`The material we process are used in the places you see below.`}
            theme={`ja-black`}
            render={(
              <div className={`products_use`}>
                <picture>
                  <source srcSet="/assets/images/products/en/car.png"
                          media="(min-width: 960px)" />
                  <img src="/assets/images/products/en/car_sp.png" alt="car"/>
                </picture>
              </div>
            )}
          />
        </section>
        <section>
          <LeadBlock01
            title={`Are any of these concerns familiar?`}
            theme={`ja-gray`}
            render={PRODUCTS_SOLUTION}
          />
        </section>
        <section>
          <LeadBlock01
            title={`Our manufacturing range`}
            theme={`ja-black`}
            render={PRODUCTS_AVAILABLE}
          />
        </section>
        <section>
          <LeadBlock01
            title={`STEEL TYPE`}
            sub={`Steel materials we handle`}
            render={PRODUCTS_TYPE}
          />
        </section>
        <SectionNext
          current={`products`}
          nextid={`05`}
          nextPath={`/en/history`}
          title={`HISTORY`}
        />
      </div>
    );
  }
}
