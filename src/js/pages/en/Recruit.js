import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"
import Modal from "react-modal"

import {interviews} from "../../data/en/interviews.json"
import {recruit} from "../../data/en/recruits.json"
import SectionHead from "../../components/section_head"
import LeadBlock01 from "../../components/leadblock_01"
import Interview from "../../components/interview_en"
import ListBlock01 from "../../components/listblock01"

const dataofModalStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

const JOINTO = (
  <div className="jointo">
    <div>
      <h3>We're looking for people who want to build the future with us.</h3>
      <p>Mishina Matsubishi will reach its 85th year of business in 2019, and we are now creating our road map for the next century,making progress on projects towards making our business plan happen. We anticipate that the industry is going to shift from mass-production systems to individually-produced products using 3D printing and other technologies. Our core competence is in changing the quality of materials (our technologies to alter the internal composition of metal) through precision rerolling technologies and heat treatment. While keeping with this, we aim to open the doors to the future of industry by developing the metallic materials that the next-generation industry will require. We need two kinds of "power" to do this:the power of engineering, where we develop new metallic materials that currently do not exist; and the power of marketing, to expand into the world. Our desire is to use these two powers to make our dreams of the future, which stem from our desire to meet the challenges, come true with you.</p>
    </div>
    <figure style={{backgroundImage: `url("/assets/images/recruit/photo01.png")`}} />
  </div>
);

const DATAOF = (cb) => (
  <div className="dataof">
    <ul>
      <li onClick={()=>{cb(0)}}/>
      <li onClick={()=>{cb(1)}}/>
      <li onClick={()=>{cb(2)}}/>
      <li onClick={()=>{cb(3)}}/>
      <li onClick={()=>{cb(4)}}/>
      <li onClick={()=>{cb(5)}}/>
      <li onClick={()=>{cb(6)}}/>
      <li onClick={()=>{cb(7)}}/>
    </ul>
  </div>
);

const INTERVIEWS = () => (
  <div className="interviews">
    {
      interviews.map((interview, i) => {
        return (
          <Interview
            key={i}
            data={interview}
            index={i}
          />
        )
      })
    }
  </div>
);

const INFORMATION = (
  <div className="infomation">
    <ListBlock01
      data={recruit}
      theme={`transparent`}
    />
    <div className={`btn__more`}>
      <a href="https://job.mynavi.jp/19/pc/search/corp215368/outline.html" target="_blank"  rel="noopener noreferrer">Apply via Mynavi</a>
    </div>
  </div>
);

Modal.setAppElement('#root')

export default class Recruit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight(),
      dataofIsOpen: false,
      dataofSelected: 0
    }
    this.updateDimensions = this.updateDimensions.bind(this);
    this.openDataofModal = this.openDataofModal.bind(this);
    this.closeDataofModal = this.closeDataofModal.bind(this);
  }

  componentWillMount(){}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  openDataofModal(data){
    if (!this.state.dataofIsOpen && this.state.width < 860) {
      this.setState({
        dataofIsOpen: true,
        dataofSelected: data
      })
    }
  }

  closeDataofModal(){
    this.setState({
      dataofIsOpen: false
    })
  }

  render() {

    let dataofModalContent = () => {
      return (
        <div className={`dataof__modal dataof-${this.state.dataofSelected+1}`}>
          <div>
            <div className="image" style={{backgroundImage: `url("/assets/images/recruit/en/sp_dataof0${this.state.dataofSelected+1}.png")`}}/>
            <button onClick={this.closeDataofModal} />
          </div>
        </div>
      )
    }

    return Style.it(`
          `,
      <div className="page recruit lang-en">
        <SectionHead
          poster={`/assets/images/recruit/h1img.png`}
          title={`CAREER`}
          sub={`Career Information`}
          theme={`white`}
        />
        <section>
          <LeadBlock01
            title={`JOIN THE MISHINA TEAM`}
            sub={`The Kind of People We're Looking For`}
            theme={`white`}
            render={JOINTO}
          />
        </section>
        <section>
          <LeadBlock01
            title={`CORPORATE DATA`}
            sub={`Mishina Matsubishi: Corporate Data`}
            theme={`white`}
            render={DATAOF(this.openDataofModal)}
          />
        </section>
        <section>
          <LeadBlock01
            title={`STAFF INTERVIEWS`}
            sub={`Interviews with Our Employees`}
            theme={`white`}
            render={INTERVIEWS()}
          />
        </section>
        <section>
          <LeadBlock01
            title={`INFORMATION`}
            sub={`Details on Current Positions Available`}
            theme={`white`}
            render={INFORMATION}
          />
        </section>
        <Modal
          className="modal-recruit"
          isOpen={this.state.dataofIsOpen}
          style={dataofModalStyles}
        >
          {dataofModalContent()}
        </Modal>
      </div>
    );
  }
}
