import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import {company_history} from "../data/company_history.json"

import SectionHead from "../components/section_head"
import SectionNext from "../components/section_next"
import LeadBlock01 from "../components/leadblock_01"
import LeadBlock02 from "../components/leadblock_02"
import ListBlock01 from "../components/listblock01"
import ColumnBlock02 from "../components/columnblock02"

const COMPANY_HISTORY = (
  <div className={`company_history`}>
    <ListBlock01
      data={company_history}
      theme={`white`}
    />
  </div>
);

const HISTORY_TITLE = (
  <span>
    今も現役のユニークな機械<br />
    それが、技術と誇りの礎。
  </span>
)

const HISTRY_COLUMNS = (
  <section>
    <ColumnBlock02
      wrap01={(
        <figure>
          <img src="/assets/images/history/photo01.png" alt="創業当初、熱田区時代の社屋外観"/>
          <figcaption>創業当初、熱田区時代の社屋外観</figcaption>
        </figure>
        )}
      wrap02={(
        <p>創業の地は名古屋市熱田区、現在の金山駅あたり。<br/>
        航空機関連の鋼板圧延加工からスタートした三品松菱の技術力が飛躍的に向上したのは、創業者の三品實が自ら設計・開発し、1961年に完成させた、「冷間二段圧延機」の誕生がきっかけでした。当時の常識だった平ベルトの代わりにVベルトを駆動に用いたユニークなこの機械は、電力事情が悪い中でも安定した駆動を実現し、関西の同業者の間で噂になるほど。</p>
      )}
      reverse={false}
    />
    <ColumnBlock02
      wrap01={(
        <figure>
          <img src="/assets/images/history/photo02.png" alt="焼鈍炉の導入によって酸洗・焼鈍・圧延という三品の3柱が揃った"/>
          <figcaption>焼鈍炉の導入によって酸洗・焼鈍・圧延という三品の3柱が揃った</figcaption>
        </figure>
      )}
      wrap02={(
        <p>開発者であり最高の腕を持つ職人でもあった三品實はこれを繊細に操り、電子制御などまったくない時代に、誤差±0.02mmという高い精度を実現したのです。それから80余年の間、小さな部品一つに至るまで純日本製のこの機械は大切に手入れされ、創業者の技と誇りを受け継ぐ職人とともに生きつづけています。</p>
      )}
      reverse={true}
    />
    <ColumnBlock02
      wrap01={(
        <figure>
          <img src="/assets/images/history/photo03.png" alt="現在も活躍する二段圧延機の原型モデル"/>
          <figcaption>現在も活躍する二段圧延機の原型モデル</figcaption>
        </figure>
      )}
      wrap02={(
        <p>1973年には生産力増強のため、主要取引先の事業所にほど近い、愛知県東郷町に本社を移転。創立50周年を迎えた1984年頃には、従来の鋼板圧延加工からコイルの圧延加工にも着手し、鋼板とコイル両方を手がけられる、全国でも数少ない圧延加工の専業メーカーとして発展する大きな礎となりました。</p>
      )}
      reverse={false}
    />
  </section>
)

export default class History extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page history">
        <SectionHead
          poster={`/assets/images/history/h1img.png`}
          title={`HISTORY`}
          sub={`三品松菱のあゆみ`}
          theme={`white`}
        />
        <section>
          <LeadBlock02
            title={HISTORY_TITLE}
            theme={`gold`}
            render={HISTRY_COLUMNS}
          />
        </section>
        <section>
          <LeadBlock01
            title={`COMPANY HISTORY`}
            sub={`沿革`}
            theme={`gold`}
            render={COMPANY_HISTORY}
          />
        </section>
        <SectionNext
          current={`history`}
          nextid={`06`}
          nextPath={`/about`}
          title={`ABOUT US`}
          sub={`会社概要`}
        />
      </div>
    );
  }
}
