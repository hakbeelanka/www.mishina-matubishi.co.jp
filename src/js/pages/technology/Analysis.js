import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>厳密な試験検査で高品質な製品を。</h3>
        <p>三品松菱では常にお客さまに満足いただけるよう、製品の品質管理を徹底。独立した検査室を設け、製品のすみずみまで厳密な検査を行っています</p>
      </div>
    )}
    reverse={true}
  />
)

const TESTING_TECHNOLOGY = (
  <div className={`columnblock03 col_3`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis02.png" alt="硬度計"/>
      </figure>
      <h3>硬度計</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis03.png" alt="万能引張試験機"/>
      </figure>
      <h3>万能引張試験機</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis04.png" alt="面粗度計"/>
      </figure>
      <h3>面粗度計</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_analysis05.png" alt="工業顕微鏡"/>
      </figure>
      <h3>工業顕微鏡</h3>
    </div>
  </div>
)

export default class Rerolling extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`analysis`}
        />
        <SectionItemHead
          poster={`/assets/images/technologies/technology/head_analysis.png`}
          title={`試験・検査`}
          sub={`安心を提供する。`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={TESTING_TECHNOLOGY}
            last={true}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
