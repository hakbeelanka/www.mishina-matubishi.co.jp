import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import LeadBlock03 from "../../components/leadblock_03"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_rerolling01.png" alt="非可逆式、2段圧延機（本社東郷工場）" />
        <figcaption>非可逆式、2段圧延機（本社東郷工場）</figcaption>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>鋼を均一・均等に延ばす技術。</h3>
        <p>鋼板・コイルをロールで挟み込み、狙い通りの厚さに延ばす圧延。仕上がり寸法が同じでも、どの厚さの材料を何回圧延するかによって、品質は大きく変わります。三品松菱では、過去の豊富なデータの蓄積に基づき、あらゆる部分で均一な厚さと組織状態となるよう工程を設計します。コンピュータによる自動測定を採り入れた最新設備による高品質・高効率生産と、熟練の職人が超高精度を実現する、一品・小ロットの「メイドインジャパン」「メイドイン三品松菱」品質のモノづくりで、お客さまの多様なご要望にお応えします。</p>
      </div>
    )}
    reverse={true}
  />
)

const REROLLING_TECHNOLOGY = (
  <div className={`columnblock03 col_4 adjust-head`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_rerolling02.png" alt="AGC"/>
      </figure>
      <h3>AGC<span>（オートギャップコントロールシステム）</span></h3>
      <p>圧延加工中のコイルの厚みを常時測定。誤差をゼロに近づけるよう、圧延ロールの隙間を自動で微調整するシステムです。</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_rerolling03.png" alt="ロールベンダーシステム"/>
      </figure>
      <h3>ロールベンダーシステム</h3>
      <p>ストレートロールで圧延するとクラウン形状になるために、ロールに適度な力を加えて矯正します。</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_rerolling04.png" alt="徹底したキズへの対策"/>
      </figure>
      <h3>徹底したキズへの対策</h3>
      <p>圧延機の出側には、裏面検査用の照明付ミラーを設置するなど、キズ流出防止策を徹底。画像処理による自動検出にも取り組んでいきます。</p>
    </div>
  </div>
)

const SPEC_TABLE = $(window).innerWidth() > 960 ?
  (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
    <table className={`spec_table`}>
      <tbody>
      <tr className="head">
        <th className="rowhead" colSpan="2"/>
        <th>製品板厚</th>
        <th>板厚精度（ご相談）</th>
        <th>幅</th>
      </tr>
      <tr>
        <th className="rowhead" rowSpan="2">豊田工場</th>
        <th className="second">第一圧延機</th>
        <td>1.2-7.0mm</td>
        <td>3.0mm以下 ±0.03mm<br/>3.0mm超 ±0.05mm</td>
        <td>65mm-330mm</td>
      </tr>
      <tr>
        <th className="second">第二圧延機</th>
        <td>0.65-8.0mm</td>
        <td>4.0mm以下 ±0.02mm<br/>4.0mm超 ±0.05mm</td>
        <td>50mm-200mm</td>
      </tr>
      <tr>
        <th className="rowhead" colSpan="2">本社工場</th>
        <td>1.2-12.0mm</td>
        <td>±0.05mm</td>
        <td>50mm-500mm</td>
      </tr>
      <tr>
        <th className="rowhead" rowSpan="2">タイ工場</th>
        <th className="second">4段ロール</th>
        <td>0.8-8.0mm</td>
        <td>4.0mm以下 ±0.02mm<br/>4.0mm超 ±0.02mm</td>
        <td>70mm-500mm</td>
      </tr>
      <tr>
        <th className="second">6段ロール</th>
        <td>0.1-1.0mm</td>
        <td>±0.007mm</td>
        <td>50mm-500mm</td>
      </tr>
      </tbody>
    </table>
  ) :
  (
    <div>
      <table className={`spec_table`}>
        <tbody>
        <tr className="head">
          <th className="rowhead" colSpan="2"/>
          <th>製品板厚</th>
          <th>板厚精度（ご相談）</th>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">豊田工場</th>
          <th className="second">第一圧延機</th>
          <td>1.2-7.0mm</td>
          <td>3.0mm以下 ±0.03mm<br/>3.0mm超 ±0.05mm</td>
        </tr>
        <tr>
          <th className="second">第二圧延機</th>
          <td>0.65-8.0mm</td>
          <td>4.0mm以下 ±0.02mm<br/>4.0mm超 ±0.05mm</td>
        </tr>
        <tr>
          <th className="rowhead" colSpan="2">本社工場</th>
          <td>1.2-12.0mm</td>
          <td>±0.05mm</td>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">タイ工場</th>
          <th className="second">4段ロール</th>
          <td>0.8-8.0mm</td>
          <td>4.0mm以下 ±0.02mm<br/>4.0mm超 ±0.02mm</td>
        </tr>
        <tr>
          <th className="second">6段ロール</th>
          <td>0.1-1.0mm</td>
          <td>±0.007mm</td>
        </tr>
        </tbody>
      </table>
      <table className={`spec_table`}>
        <tbody>
        <tr className="head">
          <th className="rowhead" colSpan="2"/>
          <th>幅</th>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">豊田工場</th>
          <th className="second">第一圧延機</th>
          <td>65mm-330mm</td>
        </tr>
        <tr>
          <th className="second">第二圧延機</th>
          <td>50mm-200mm</td>
        </tr>
        <tr>
          <th className="rowhead" colSpan="2">本社工場</th>
          <td>50mm-500mm</td>
        </tr>
        <tr>
          <th className="rowhead" rowSpan="2">タイ工場</th>
          <th className="second">4段ロール</th>
          <td>70mm-500mm</td>
        </tr>
        <tr>
          <th className="second">6段ロール</th>
          <td>50mm-500mm</td>
        </tr>
        </tbody>
      </table>
    </div>
  );


export default class Rerolling extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`rerolling`}
        />
        <SectionItemHead
          poster={`/assets/videos/atsuen.mp4`}
          title={`圧延`}
          sub={`厚みを操る。`}
          select={`analysis`}
        />
        <section>
          <LeadBlock02
            theme={`gold`}
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={REROLLING_TECHNOLOGY}
          />
        </section>
        <section>
          <LeadBlock03
            title={"キズ防止対策（コイル）"}
          >
            <picture>
              <img src="/assets/images/technologies/technology/photo_rerolling05.png" alt="キズ防止対策（コイル）" />
            </picture>
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock03
            title={"圧延スペック"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList/>
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
