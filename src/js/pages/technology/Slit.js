import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import LeadBlock03 from "../../components/leadblock_03"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_slit01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>ISOに基づく確かな品質をお届け。</h3>
        <p>コイルの横幅を切り分けるスリット加工。10ミクロン単位の精密仕上げを行うと同時に、サンプリングによる寸法・硬度・面粗度のチェックや、ベテランスタッフの目視によるキズの再確認も実施。ISO9001の品質マネジメントシステムに基づいた管理で、確かな品質をお届けします。</p>
      </div>
    )}
    reverse={true}
  />
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table`}>
    <tbody>
    <tr className="head">
      <th className="rowhead" />
      <th>素材板厚</th>
      <th>素材幅</th>
      <th>内径</th>
      <th>外径（max）</th>
      <th>重量（max）</th>
    </tr>
    <tr>
      <th className="rowhead">豊田工場</th>
      <td>2.0-6.0mm</td>
      <td>50-300mm</td>
      <td>Φ508mm</td>
      <td>Φ1500mm</td>
      <td>2,800kg</td>
    </tr>
    <tr>
      <th className="rowhead">タイ工場</th>
      <td>1.0-10.0mm</td>
      <td>50-500mm</td>
      <td>Φ508,610mm</td>
      <td>Φ1600mm</td>
      <td>8,000kg</td>
    </tr>
    </tbody>
  </table>
)


export default class Slit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`slit`}
        />
        <SectionItemHead
          poster={`/assets/videos/slit.mp4`}
          title={`スリット`}
          sub={`鉄を裂く。`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock03
            title={"スリッタースペック"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
