import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import LeadBlock03 from "../../components/leadblock_03"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>板金加工で使いやすさを向上。</h3>
        <p>各種穴あけ加工、パンチング、スポット溶接などさまざまな板金加工を承ります。 詳細はご相談ください。</p>
      </div>
    )}
    reverse={true}
  />
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table col_4`}>
    <tbody>
    <tr className="head">
      <th>設備名</th>
      <th>呼び名称</th>
      <th>投入材寸法＆適用内容</th>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="3">シャーリング</th>
      <th>大型シャーリング</th>
      <td>1.2-7.0mm</td>
    </tr>
    <tr>
      <th>中型シャーリング</th>
      <td>1.2-7.0mm</td>
    </tr>
    <tr>
      <th>小型シャーリング</th>
      <td>1.2-7.0mm</td>
    </tr>
    <tr>
      <th className="rowhead">パンチングマシン</th>
      <td />
      <td className="borderleft">
        投入材大きさ=1000 x 2540mm<br />
        板厚=max4.5mm<br />
        （機能）丸、四角等色々な形状の穴を開けたり、切断加工をする設備である。
      </td>
    </tr>
    <tr>
      <th className="rowhead">ベンディングマシン</th>
      <td />
      <td className="borderleft">
        ベンディング部長さ=2000mm<br />
        板厚=max3.2mm<br />
        （機能）0~180°の角曲げ、R曲げ加工が出来その延長としてコ字加工も可である。
      </td>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="2">プレス</th>
      <td>30tonメカプレス</td>
      <td className="borderleft" rowSpan="2">穴、曲げ、切断に適用する。</td>
    </tr>
    <tr>
      <td>60tonメカプレス</td>
    </tr>
    <tr>
      <th className="rowhead">その他加工機器</th>
      <td colSpan="2">
        タップドリル機<br />
        ボール盤<br />
        バンドペーパー機<br />
        コンターマシン　他
      </td>
    </tr>
    </tbody>
  </table>
)

const SHEETMETAL_TECHNOLOGY = (
  <div className={`columnblock03 col_3`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal02.png" alt="各種穴あけ加工" />
      </figure>
      <h3>各種穴あけ加工</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal03.png" alt="パンチングと曲げの組み合わせ" />
      </figure>
      <h3>パンチングと曲げの組み合わせ</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal04.png" alt="曲げ加工とスポット溶接の組み合わせ" />
      </figure>
      <h3>曲げ加工とスポット溶接の組み合わせ</h3>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_sheetmetal05.png" alt="局部の2段曲げと溶接の組み合わせ" />
      </figure>
      <h3>局部の2段曲げと溶接の組み合わせ</h3>
    </div>
  </div>
)


export default class SteelMetal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`sheetmetal`}
        />
        <SectionItemHead
          poster={`/assets/images/technologies/technology/head_sheetmetal.png`}
          title={`板金加工`}
          sub={`形状を変化。`}
          select={`analysis`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={SHEETMETAL_TECHNOLOGY}
          />
        </section>
        <section>
          <LeadBlock03
            title={"加工設備明細"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
