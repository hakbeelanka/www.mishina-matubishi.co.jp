import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_pickling01.png" alt="鋼板ハンドリングロボットが自動で1点ずつカートリッジにセット"/>
        <figcaption>鋼板ハンドリングロボットが自動で1点ずつカートリッジにセット。</figcaption>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>液体の硫酸によりスケールを除去。</h3>
        <p>素材に付着したスケール（酸化皮膜）を落とす酸洗。液体の硫酸を用いるため、ショットブラスト※に比べ、微細な凹みの内側のスケールまで徹底的に除去できるのが特長です。長尺4mの切板まで対応可能な大型槽を完備。鋼板の取り回しの際にはロボットシステムを使用するなど、人の経験やスキルを必要としない作業は、積極的に省力化を図っています。使用後の廃液から、沈澱した鉄粉を取り出し資源化するリサイクルにも取り組んでいます。<span>※ショットブラスト／細かい金属の粒体を高速で衝突させてスケールを除去する表面処理方法</span></p>
      </div>
    )}
    reverse={true}
  />
)

const CUTTING_TECHNOLOGY = (
  <div className={`columnblock03 col_6`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_pickling02.png" alt="硫酸槽、水洗槽、中和槽の3つの槽で順に処理。いずれも長尺4mまでの鋼板に対応できる大型槽です。"/>
      </figure>
      <p>硫酸槽、水洗槽、中和槽の3つの槽で順に処理。いずれも長尺4mまでの鋼板に対応できる大型槽です。</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_pickling03.png" alt="鋼板表面の「使えない」部分を、硫酸ではがして除去。微細な凹み部分にも、液体の硫酸が届きます。"/>
      </figure>
      <p>鋼板表面のスケール（酸化皮膜）部分を、硫酸ではがして除去。微細な凹み部分にも、液体の硫酸が届きます。</p>
    </div>
  </div>
)

export default class Pickling extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`pickling`}
        />
        <SectionItemHead
          poster={`/assets/videos/sansen_1.mp4`}
          title={`酸洗`}
          sub={`酸でみがく。`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={CUTTING_TECHNOLOGY}
            last={true}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
