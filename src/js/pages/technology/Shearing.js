import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_shearing01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>独自設備が高精度の切断を実現。</h3>
        <p>鋼板を刃で切断するシャーリング。三品松菱では、万全のキズ対策を施したオリジナル切断機を使用しています。また、製品を吸盤・磁石で移動・集積するロボットを導入し、工程間におけるキズ防止にも配慮。さらに、圧延前の粗切断の際に、鋼板に反りやひねりが発生しないよう設備を工夫し、精度の向上を図っています。</p>
      </div>
    )}
    reverse={true}
  />
)

const SHEARING_TECHNOLOGY = (
  <div className={`columnblock03 col_6`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_shearing02.png" alt=""/>
      </figure>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_shearing03.png" alt=""/>
      </figure>
    </div>
  </div>
)

export default class Shearing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`shearing`}
        />
        <SectionItemHead
          poster={`/assets/videos/sharing.mp4`}
          title={`シャーリング`}
          sub={`鉄を断つ。`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={SHEARING_TECHNOLOGY}
            last={true}
          />
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
