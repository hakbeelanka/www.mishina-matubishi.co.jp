import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import LeadBlock03 from "../../components/leadblock_03"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_annealing01.png" alt="豊田第一工場では、ピット型焼鈍炉を導入しています。" />
        <figcaption>豊田第一工場では、ピット型焼鈍炉を導入しています。</figcaption>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>炭化物を分散させ狙い通りの硬度に。</h3>
        <p>我々の技術の真髄ともいえる焼鈍（焼きなまし）。熱処理を行うことにより、組織内部のパーライトを球状（セメンタイト）化して分散させ、目指す組織状態と硬度を持つ鋼材に「改質」します。これにより、お客さまのもとでプレス・切削・メッキ等を行う際の加工性を向上させ、焼き入れ時の均一性を達成します。三品松菱が長年の経験の中で編み出してきた温度条件による無酸化光輝焼鈍を行っています</p>
      </div>
    )}
    reverse={true}
  />
)

const ANNEALING_FIGURES = (
  <div className={`columnblock03 col_8-4`}>
    <div>
      <div className={`big`}>
        <div>
          <figure>
            <img src="/assets/images/technologies/technology/photo_annealing02.png" alt="焼鈍前" />
          </figure>
          <h3>焼鈍前</h3>
        </div>
        <div>
          <figure>
            <img src="/assets/images/technologies/technology/photo_annealing03.png" alt="焼鈍後" />
          </figure>
          <h3>焼鈍後</h3>
        </div>
      </div>
      <p>焼鈍前（左）の金属組織の中に見える黒い固まりが、炭化物（セメンタイト）。球状化焼鈍により、この固まりが細かく砕かれて一様に球状化（右）します。</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_annealing04.png" alt="大型の焼鈍炉" />
      </figure>
      <h3>大型の焼鈍炉</h3>
      <p>本社東郷工場では、大型の焼鈍炉を導入。鋼板を中心に、コイルの焼鈍にも対応可能です。</p>
    </div>
  </div>
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table col_5`}>
    <tbody>
    <tr className="head">
      <th className="rowhead"/>
      <th>素材</th>
      <th>コイル外径（max）</th>
      <th>鋼板（max）</th>
      <th>重量</th>
    </tr>
    <tr>
      <th className="rowhead">豊田工場</th>
      <td>鋼帯専用</td>
      <td>Φ1380mm</td>
      <td />
      <td>6,000kg/3基</td>
    </tr>
    <tr>
      <th className="rowhead">本社工場</th>
      <td>鋼板・鋼帯専用</td>
      <td>Φ1400mm</td>
      <td>1400(W) x 3900(L) x 700(T)</td>
      <td>15,000kg</td>
    </tr>
    <tr>
      <th className="rowhead">タイ工場</th>
      <td>鋼帯専用</td>
      <td>Φ1600mm</td>
      <td />
      <td>10,000kg/3基</td>
    </tr>
    </tbody>
  </table>
)

export default class Annealing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`annealing`}
        />
        <SectionItemHead
          poster={`/assets/videos/syoudon.mp4`}
          title={`焼鈍`}
          sub={`熱を制する。`}
        />
        <section>
          <LeadBlock02
            theme={`gold`}
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={ANNEALING_FIGURES}
          >
          </LeadBlock02>

        </section>
        <section>
          <LeadBlock03
            title={"焼鈍スペック"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
