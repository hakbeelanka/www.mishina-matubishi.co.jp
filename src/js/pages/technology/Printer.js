import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import LeadBlock03 from "../../components/leadblock_03"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_printer01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>自由な造形が可能性を広げる。</h3>
        <p>3Dプリンターは鋳型や治具の製造を必要とせず、複雑な形状も迅速に造形できることから、設計段階の試作やテストに向いています。試作品のご用命など、お気軽にお問い合わせください。また、3Dプリンターを使った部品製造の研究・開発も進めています。</p>
      </div>
    )}
    reverse={true}
  />
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table`}>
    <tbody>
    <tr>
      <th className="rowhead">造形サイズ</th>
      <td>254(W) x 254(D) x 254(H)mm</td>
    </tr>
    <tr>
      <th className="rowhead">造形材料</th>
      <td>ASA/ABS-M30/PLA</td>
    </tr>
    <tr>
      <th className="rowhead">ポート材料</th>
      <td>WaterWorksソリュブルサポート方式</td>
    </tr>
    <tr>
      <th className="rowhead">積層ピッチ</th>
      <td>0.127/0.178/0.254/0.330mm</td>
    </tr>
    <tr>
      <th className="rowhead">精度</th>
      <td>±0.200mm または ±0.002mm</td>
    </tr>
    </tbody>
  </table>
)


export default class Printer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`printer`}
        />
        <SectionItemHead
          poster={`/assets/images/technologies/technology/head_printer.png`}
          title={`3Dプリンター`}
          sub={`巧みな三次元造形。`}
          select={`analysis`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock03
            title={"製品の特徴"}
            render={SPEC_TABLE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
