import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"

import SectionHeadTechnology from "../../components/section_head_technology"
import SectionItemHead from "../../components/section_itemhead"
import LeadBlock01 from "../../components/leadblock_01"
import LeadBlock02 from "../../components/leadblock_02"
import LeadBlock03 from "../../components/leadblock_03"
import ColumnBlock02 from "../../components/columnblock02"
import TechnologyList from "../../components/technologyList"

const MAIN = (
  <ColumnBlock02
    wrap01={(
      <figure>
        <img src="/assets/images/technologies/technology/photo_cutting01.png" alt=""/>
      </figure>
    )}
    wrap02={(
      <div>
        <h3>プレスカットにより型抜きも視野に。</h3>
        <p>コイルから鋼板を切り出すレベラーカット工程。レベラーによってコイルの巻き癖を矯正し、まっすぐ平坦な状態にしたうえでカットします。プレスカット方式とフライングシャー方式の2ラインを有し、9ミリ超という厚い鋼板のカットも可能です。さらに、プレスカット機の応用により、自在な形状に型抜きをするブランキング加工も視野に入れています。過去にプレスによる自転車部品の製造を行った歴史を持つ三品松菱には、プレス加工の実績もあります。</p>
      </div>
    )}
    reverse={true}
  />
)

const CUTTING_TECHNOLOGY = (
  <div className={`columnblock03 col_6`}>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_cutting02.png" alt="厚物切断用のプレスカット機は、金型を付け替えれば、自在に型抜きするブランキング加工も可能。"/>
      </figure>
      <p>厚物切断用のプレスカット機は、金型を付け替えれば、自在に型抜きするブランキング加工も可能。</p>
    </div>
    <div>
      <figure>
        <img src="/assets/images/technologies/technology/photo_cutting03.png" alt="大小約11個のローラーから成るレベラー。繊細な力加減で、曲がりをていねいに矯正していきます。"/>
      </figure>
      <p>大小のローラーから成るレベラー。繊細な力加減で、曲がりをていねいに矯正していきます。</p>
    </div>
  </div>
)

const SPEC_TABLE = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table`}>
    <tbody>
    <tr className="head">
      <th className="rowhead" colSpan="2"/>
      <th>素材板厚</th>
      <th>素材幅</th>
      <th>内径</th>
      <th>外径（max）</th>
      <th>重量（max）</th>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="2">豊田工場</th>
      <th className="second">第一レベラー</th>
      <td>2.0-6.0mm</td>
      <td>80-350mm</td>
      <td>Φ508mm</td>
      <td>Φ1500mm</td>
      <td>2,800kg</td>
    </tr>
    <tr>
      <th className="second">第二レベラー</th>
      <td>2.0-9.0mm</td>
      <td>50-400mm</td>
      <td>Φ508,Φ610mm</td>
      <td>Φ1500mm</td>
      <td>3,000kg</td>
    </tr>
    <tr>
      <th className="rowhead" colSpan="2">タイ工場</th>
      <td>1.0-10.0mm</td>
      <td>70-500mm</td>
      <td>Φ508,Φ610mm</td>
      <td>Φ1600mm</td>
      <td>8,000kg</td>
    </tr>
    </tbody>
  </table>
)

const SPEC_TABLE2 = (
  // FIXME: ほんとはデータを元にテーブルが作られるべき
  <table className={`spec_table adjust_cutting_table`}>
    <tbody>
    <tr className="head">
      <th className="rowhead" colSpan="2"/>
      <th>製品長さ</th>
    </tr>
    <tr>
      <th className="rowhead" rowSpan="2">豊田工場</th>
      <th className="second">第一レベラー</th>
      <td>700-3000mm</td>
    </tr>
    <tr>
      <th className="second">第二レベラー</th>
      <td>65-3400mm</td>
    </tr>
    <tr>
      <th className="rowhead" colSpan="2">タイ工場</th>
      <td>50-3000mm</td>
    </tr>
    </tbody>
  </table>
)

const TABLE_COMBINE = (
  <div>
    {SPEC_TABLE}
    {SPEC_TABLE2}
  </div>
)

export default class Cutting extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight()
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentWillMount() {

  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  render() {

    return Style.it(`
          `,
      <div className="page technologies">
        <SectionHeadTechnology
          icon={`cutting`}
        />
        <SectionItemHead
          poster={`/assets/videos/leberacut.mp4`}
          title={`レベラーカット`}
          sub={`多彩な切り出し。`}
        />
        <section>
          <LeadBlock02
            render={MAIN}
          />
        </section>
        <section>
          <LeadBlock02
            render={CUTTING_TECHNOLOGY}
          />
        </section>
        <section>
          <LeadBlock03
            title={"レベラースペック"}
            render={TABLE_COMBINE}
            last={true}
          >
          </LeadBlock03>
        </section>
        <section>
          <LeadBlock01
            title={`TECHNOLOGY`}
            sub={`技術紹介`}
            theme={`black`}
          >
            <TechnologyList />
          </LeadBlock01>
        </section>
      </div>
    );
  }
}
