import React, {Component} from "react"
import $ from "jquery"
import Style from "style-it"
import CountUp from "react-countup"
import Modal from "react-modal"

import {interviews} from "../data/interviews.json"
import {recruit} from "../data/recruits.json"
import {dataof} from "../data/recruit_dataof.json"

import SectionHead from "../components/section_head"
import LeadBlock01 from "../components/leadblock_01"
import Interview from "../components/interview"
import ListBlock01 from "../components/listblock01"

const dataofModalStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

const JOINTO = (
  <div className="jointo">
    <div>
      <h3>ともに未来を築く人を<br />募集しています。</h3>
      <p>2019年に創業85年を迎える三品松菱では、次の100年に向けた事業構想を策定。その実現に向けたプロジェクトが現在進行中です。世の中の産業構造は、これまでの「大量生産方式」から、3Dプリンターなどを活用した「個別生産方式」に変化することが予測されます。私たちは、これまで培ってきた精密圧延技術・熱処理による「改質（金属内部組織改変技術）」をコアコンピタンスとしながらも、次世代の産業に求められる金属材料を開発していくことで、産業の未来を切り拓いていくことを目指しています。<br /> その未来を実現するためには、2つのパワーが必要です。これまで存在しなかった新しい金属材料を開発していくエンジニアリングパワーと、それを世界に広めるマーケティングパワー。これからの三品松菱に必要な２つのパワーをもち、チャレンジ精神の旺盛なあなたと、いっしょに未来を叶えていきたいと思います。</p>
    </div>
    <figure style={{backgroundImage: `url("/assets/images/recruit/photo01.png")`}} />
  </div>
);

const DATAOF = (cb) => (
  <div className="dataof">
    <ul>
      <li onClick={()=>{cb(0)}} data-of={dataof[0].content}><p>
        <CountUp ref={(countup) => { console.log(countup)}} start={0} end={35} duration={3}/><span>万</span></p></li>
      <li onClick={()=>{cb(1)}} data-of={dataof[1].content}><p>
        <span>究極の</span><span>一個</span><span>づくり</span></p></li>
      <li onClick={()=>{cb(2)}} data-of={dataof[2].content}><p>
        <CountUp ref={(countup) => { console.log(countup)}} start={0} end={15} duration={3} /><span>時間</span></p></li>
      <li onClick={()=>{cb(3)}} data-of={dataof[3].content}><p>
        <span>海外</span><span>展開</span></p></li>
      <li onClick={()=>{cb(4)}} data-of={dataof[4].content}><p>
        <span>鉄を</span><span>究める</span></p></li>
      <li onClick={()=>{cb(5)}} data-of={dataof[5].content}><p>
        <CountUp ref={(countup) => { console.log(countup)}} start={0} end={9} duration={3} />:<CountUp ref={(countup) => { console.log(countup)}} start={0} end={1} duration={3} /></p></li>
      <li onClick={()=>{cb(6)}} data-of={dataof[6].content}><p>
        <span>新素材</span><span>開発</span></p></li>
      <li onClick={()=>{cb(7)}} data-of={dataof[7].content}><p>
        <span>3D</span></p></li>
    </ul>
  </div>
);

const INTERVIEWS = () => (
  <div className="interviews">
    {
      interviews.map((interview, i) => {
        return (
          <Interview
            key={i}
            data={interview}
            index={i}
          />
        )
      })
    }
  </div>
);

const INFORMATION = (
  <div className="infomation">
    <ListBlock01
      data={recruit}
      theme={`transparent`}
    />
    <div className={`btn__more`}>
      <a href="https://job.mynavi.jp/19/pc/search/corp215368/outline.html" target="_blank"  rel="noopener noreferrer">マイナビでエントリー</a>
    </div>
  </div>
);

Modal.setAppElement('#root')

export default class Recruit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      width: $(window).innerWidth(),
      height: $(window).innerHeight(),
      dataofIsOpen: false,
      dataofSelected: 0
    }
    this.updateDimensions = this.updateDimensions.bind(this);
    this.openDataofModal = this.openDataofModal.bind(this);
    this.closeDataofModal = this.closeDataofModal.bind(this);
  }

  componentWillMount(){　}

  componentDidMount(){
    window.addEventListener("resize", this.updateDimensions)
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions() {
    this.setState({width: $(window).innerWidth(), height: $(window).innerHeight()});
  }

  openDataofModal(data){
    if (!this.state.dataofIsOpen && this.state.width < 860) {
      this.setState({
        dataofIsOpen: true,
        dataofSelected: data
      })
    }
  }

  closeDataofModal(){
    this.setState({
      dataofIsOpen: false
    })
  }

  render() {

    let dataofModalContent = () => {
      return (
        <div className={`dataof__modal dataof-${this.state.dataofSelected+1}`}>
          <div>
            <div className="image" style={{backgroundImage: `url("/assets/images/recruit/sp_dataof0${this.state.dataofSelected+1}.png")`}}/>
            <button onClick={this.closeDataofModal} />
          </div>
        </div>
      )
    }

    return Style.it(`
          `,
      <div className="page recruit">
        <SectionHead
          poster={`/assets/images/recruit/h1img.png`}
          title={`RECRUIT`}
          sub={`採用情報`}
          theme={`white`}
        />
        <section>
          <LeadBlock01
            title={`JOIN THE MISHINA TEAM`}
            sub={`求める人物像`}
            theme={`white`}
            render={JOINTO}
          />
        </section>
        <section>
          <LeadBlock01
            title={`CORPORATE DATA`}
            sub={`データで見る三品松菱`}
            theme={`white`}
            render={DATAOF(this.openDataofModal)}
          />
        </section>
        <section>
          <LeadBlock01
            title={`STAFF INTERVIEWS`}
            sub={`社員インタビュー`}
            theme={`white`}
            render={INTERVIEWS()}
          />
        </section>
        <section>
          <LeadBlock01
            title={`INFORMATION`}
            sub={`募集要項`}
            theme={`white`}
            render={INFORMATION}
          />
        </section>
        {/*<SectionNext*/}
          {/*current={`recruit`}*/}
          {/*nextid={`08`}*/}
          {/*title={`FUTURE`}*/}
          {/*sub={`発動！未来創造プロジェクト`}*/}
        {/*/>*/}
        <Modal
          className="modal-recruit"
          isOpen={this.state.dataofIsOpen}
          style={dataofModalStyles}
        >
          {dataofModalContent()}
        </Modal>
      </div>
    );
  }
}
