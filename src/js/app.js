import React, {Component} from "react"
import {Route} from "react-router-dom"
import TransitionGroup from "react-transition-group/TransitionGroup"
import AnimatedSwitch from "./components/animated_switch"
import ScrollTop from "./components/scrolltop_wrapper"

import HeaderMenu from "./components/header_menu"
import Footer from "./components/footer"
import BackToTop from "./components/backtotop"
import LanguageSwitcher from "./components/languageSwitcher"

import Home from "./pages/Home"
import Vision from "./pages/Vision"
import Philosophy from "./pages/Philosophy"
import Technologies from "./pages/Technologies"
import Products from "./pages/Products"
import History from "./pages/History";
import About from "./pages/About"
import Recruit from "./pages/Recruit"

import Rerolling from "./pages/technology/Rerolling"
import Annealing from "./pages/technology/Annealing"
import Pickling from "./pages/technology/Pickling"
import Cutting from "./pages/technology/Cutting"
import Shearing from "./pages/technology/Shearing"
import Slit from "./pages/technology/Slit"
import Printer from "./pages/technology/Printer"
import Analysis from "./pages/technology/Analysis"
import SteelMetal from "./pages/technology/SteelMetal"

import HomeEn from "./pages/en/Home"
import VisionEn from "./pages/en/Vision"
import PhilosophyEn from "./pages/en/Philosophy"
import TechnologiesEn from "./pages/en/Technologies"
import ProductsEn from "./pages/en/Products"
import HistoryEn from "./pages/en/History";
import AboutEn from "./pages/en/About"
import RecruitEn from "./pages/en/Recruit"

import RerollingEn from "./pages/en/technology/Rerolling"
import AnnealingEn from "./pages/en/technology/Annealing"
import PicklingEn from "./pages/en/technology/Pickling"
import CuttingEn from "./pages/en/technology/Cutting"
import ShearingEn from "./pages/en/technology/Shearing"
import SlitEn from "./pages/en/technology/Slit"
import PrinterEn from "./pages/en/technology/Printer"
import AnalysisEn from "./pages/en/technology/Analysis"
import SteelMetalEn from "./pages/en/technology/SteelMetal"

import Missed from "./pages/404"

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      menu: [
        {"n": "PHILOSOPHY", "t": "/philosophy", "s": "私たちの鉄則"},
        {"n": "VISION", "t": "/vision", "s": "未来への展望"},
        {"n": "TECHNOLOGY", "t": "/technology", "s": "技術紹介"},
        {"n": "PRODUCTS", "t": "/products", "s": "製品について"},
        {"n": "HISTORY", "t": "/history", "s": "三品松菱のあゆみ"},
        {"n": "ABOUT US", "t": "/about", "s": "会社概要"},
        {"n": "RECRUIT", "t": "/recruit", "s": "採用情報"},
        // {"n": "FUTURE", "t": "/future", "s": "未来創造プロジェクト"}
      ],
      lang: "/"
    }

    this.languageSwitch = this.languageSwitch.bind(this);
  }

  componentDidMount(){
    this.setState({
      lang: window.location.pathname.indexOf('/en/') === -1 ? '/' : '/en/'
    })
  }

  languageSwitch(path){
    this.setState({
      lang: path
    })
  }

  render() {
    return (
      <div className={`wrapper ${this.state.lang === '/' ? 'lang-ja': 'lang-en'}`}>
        <HeaderMenu
          menu={this.state.menu}
          lang={this.state.lang}
        >
          <LanguageSwitcher handler={this.languageSwitch}
                            lang={this.state.lang}
                            {...this.props}
          />
        </HeaderMenu>
        <Route
          render={({location}) => (
            <ScrollTop>
              <TransitionGroup component="main" className="main">
                <AnimatedSwitch
                  key={location.key}
                  location={location}
                >
                  <Route exact path="/"
                         render={props => ( <Home {...props} /> )}/>
                  <Route exact path="/about"
                         render={props => ( <About {...props} /> )}/>
                  <Route exact path="/vision"
                         render={props => ( <Vision {...props} /> )}/>
                  <Route exact path="/philosophy"
                         render={props => ( <Philosophy {...props} /> )}/>
                  <Route exact path="/technologies/"
                         render={props => ( <Technologies {...props}/>)}/>
                  <Route exact path="/technologies/rerolling"
                         render={props => ( <Rerolling {...props}/>)}/>
                  <Route exact path="/technologies/annealing"
                         render={props => ( <Annealing {...props}/>)}/>
                  <Route exact path="/technologies/pickling"
                         render={props => ( <Pickling {...props} /> )}/>
                  <Route exact path="/technologies/cutting"
                         render={props => ( <Cutting {...props} />)}/>
                  <Route exact path="/technologies/shearing"
                         render={props => (<Shearing{...props}/>)}/>
                  <Route exact path="/technologies/slit"
                         render={props => (<Slit{...props}/>)}/>
                  <Route exact path="/technologies/printer"
                         render={props => (<Printer{...props}/>)}/>
                  <Route exact path="/technologies/analysis"
                         render={props => (<Analysis{...props}/>)}/>
                  <Route exact path="/technologies/sheetmetal"
                         render={props => (<SteelMetal{...props}/>)}/>
                  <Route exact path="/recruit"
                         render={props => (<Recruit{...props}/>)}/>
                  <Route exact path="/products"
                         render={props => (<Products{...props}/>)}/>
                  <Route exact path="/history"
                         render={props => (<History{...props}/>)}/>

                  <Route exact path="/en"
                         render={props => ( <HomeEn {...props} /> )}/>
                  <Route exact path="/en/about"
                         render={props => ( <AboutEn {...props} /> )}/>
                  <Route exact path="/en/vision"
                         render={props => ( <VisionEn {...props} /> )}/>
                  <Route exact path="/en/philosophy"
                         render={props => ( <PhilosophyEn {...props} /> )}/>
                  <Route exact path="/en/technologies/"
                         render={props => ( <TechnologiesEn {...props}/>)}/>
                  <Route exact path="/en/technologies/rerolling"
                         render={props => ( <RerollingEn {...props}/>)}/>
                  <Route exact path="/en/technologies/annealing"
                         render={props => ( <AnnealingEn {...props}/>)}/>
                  <Route exact path="/en/technologies/pickling"
                         render={props => ( <PicklingEn {...props} /> )}/>
                  <Route exact path="/en/technologies/cutting"
                         render={props => ( <CuttingEn {...props} />)}/>
                  <Route exact path="/en/technologies/shearing"
                         render={props => (<ShearingEn {...props}/>)}/>
                  <Route exact path="/en/technologies/slit"
                         render={props => (<SlitEn {...props}/>)}/>
                  <Route exact path="/en/technologies/printer"
                         render={props => (<PrinterEn {...props}/>)}/>
                  <Route exact path="/en/technologies/analysis"
                         render={props => (<AnalysisEn {...props}/>)}/>
                  <Route exact path="/en/technologies/sheetmetal"
                         render={props => (<SteelMetalEn {...props}/>)}/>
                  <Route exact path="/en/recruit"
                         render={props => (<RecruitEn {...props}/>)}/>
                  <Route exact path="/en/products"
                         render={props => (<ProductsEn {...props}/>)}/>
                  <Route exact path="/en/history"
                         render={props => (<HistoryEn {...props}/>)}/>
                  <Route component={Missed}/>
                </AnimatedSwitch>
              </TransitionGroup>
            </ScrollTop>
          )}
        />
        <BackToTop />
        <Footer lang={this.state.lang}/>
      </div>
    );
  }
}